<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" type="text/css" href="{{ url('../css/myStyle.css') }}">
     <script src="https://kit.fontawesome.com/091cd4a75a.js" crossorigin="anonymous"></script>
     <title>MRVN | LOGIN</title>
</head>
<body>
     <div class="navbar">
          <div>
               <img src="../images/logo.svg" class="logo">
          </div>

          <div class="links">
               <a href="{{ url ('/home')}}"><div>HOME</div></a>
               <a href="{{ url ('/about')}}"><div>ABOUT ME</div></a>
               <a href="{{ url ('/gallery')}}"><div>GALLERY</div></a>
               <a href="{{ url ('/login')}}"><div>LOGIN</div></a>
          </div>
     </div>

     <div class="section-header">
          <div class="section-title">
               <h1>REGISTER</h1>
          </div>
     </div>

     <div class="container-more" id="container-more">
               <div class="form">
                    <form>
                         <div>
                              <input type="text" class="input-form" placeholder="Name" required>
                         </div>

                         <div>
                              <input type="text" class="input-form" placeholder="Email" required>
                         </div>

                         <div>
                              <input type="password" class="input-form" placeholder="Password" required>
                         </div>

                         <div>
                              <input type="password" class="input-form" placeholder="Confirm password" required>
                         </div>

                         <div>
                              <button class="btn-send">SEND</button>
                         </div>

                         <div style="margin-top: 20px;">
                              Already have an account? <a href="{{ url ('/login')}}">LOGIN</a>
                         </div>
                    </form>
               </div>
          </div>
     </div>

     <div class="footer">
          <div class="footer-container">
               <div>
                    <h1>DON'T MISS A BEAT!</h1>
               </div>

               <div class="sns">
                    <div><i class="fa-brands fa-facebook-f"></i></div>
                    <div><i class="fa-brands fa-instagram"></i></div>
                    <div><i class="fa-brands fa-twitter"></i></div>
                    <div><i class="fa-brands fa-linkedin-in"></i></div>
               </div>

               <div>
                    <img src="../images/logo.svg">
               </div>
          </div>
     </div>
</body>
</html>