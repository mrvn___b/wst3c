<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" type="text/css" href="{{ url('../css/myStyle.css') }}">
     <script src="https://kit.fontawesome.com/091cd4a75a.js" crossorigin="anonymous"></script>
     <title>MRVN | HOME</title>
</head>
<body>
     <div class="navbar">
          <div>
               <img src="../images/logo.svg" class="logo">
          </div>

          <div class="links">
               <a href="{{ url ('/home')}}"><div>HOME</div></a>
               <a href="{{ url ('/about')}}"><div>ABOUT ME</div></a>
               <a href="{{ url ('/gallery')}}"><div>GALLERY</div></a>
               <a href="{{ url ('/login')}}"><div>LOGIN</div></a>
          </div>
     </div>

     <div class="container-main">
          <div class="main-content">
               <div class="main-heading">
                    Hi there! <br/> I'm Marvin.<br></div>

               <div class="separator"></div>

               <div class="sub-heading">
                    Let's get to know each other!
               </div>

               <a class="main-button" href="#container-more">Sure, Let's go!</a>
          </div>
     </div>

     <div class="container-more" id="container-more">
          <div class="container-content">
               <h1 class="title">MEANING OF MINE</h1>

               <div class="title-separator"></div>

               <div class="content">
                    <div>
                         <p>
                              Have you ever wonder what is the meaning of your given name? 
                              Out of curiosity, I search about the meaning of my own name.
                              <br>
                              <br>
                         </p>

                         <p>
                              The name Marvin is primarily a male name of English origin that means <i>Great Lord</i>.
                              <br>
                              <br>
                         </p>        

                         <p>
                              My parents told me that my name was given by the midwife who helped my mother from giving birth to me. 
                              It is just interesting to know how you parents gave you your name right?
                              <br>
                              <br>
                         </p>

                         <p>
                              How about you? What is the meaning of your name and how did you get your name?
                         </p>
                    </div>

                    <div>
                         <img class="image-content" src="../images/pic1.jpg">
                    </div>
               </div>
          </div>
     </div>

     <div class="short-container">
          <div class="quote">
               <h1>SNIPPET ABOUT ME</h1>
               <br>
               <p>
                    If you are asking me about my hobbies, well<br>
                    When I was a highschooler, I like playing volleyball<br>
                    But when I started college, I barely play volleyball with my friends<br>
                    I also do portrait sketching, cooking,<br>
                    surfing the internet, reading books and articles online<br>
                    As of now, when I have a free time, I am watching North Korean documentaries,<br>
                    reading articles about North Korea and many more related to it<br>
                    My friends tought of me as a wierdo sometimes because I am addicted into reading and <br>
                    watching documentaries which has nothing to do with me or my course<br>
               </p>
          </div>
     </div>

     <div class="footer">
          <div class="footer-container">
               <div>
                    <h1>DON'T MISS A BEAT!</h1>
               </div>

               <div class="sns">
                    <div><i class="fa-brands fa-facebook-f"></i></div>
                    <div><i class="fa-brands fa-instagram"></i></div>
                    <div><i class="fa-brands fa-twitter"></i></div>
                    <div><i class="fa-brands fa-linkedin-in"></i></div>
               </div>

               <div>
                    <img src="../images/logo.svg">
               </div>
          </div>
     </div>

</body>
</html>