<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" type="text/css" href="{{ url('../css/myStyle.css') }}">
     <script src="https://kit.fontawesome.com/091cd4a75a.js" crossorigin="anonymous"></script>
     <title>MRVN | GALLERY</title>
</head>
<body>
     <div class="navbar">
          <div>
               <img src="../images/logo.svg" class="logo">
          </div>

          <div class="links">
               <a href="{{ url ('/home')}}"><div>HOME</div></a>
               <a href="{{ url ('/about')}}"><div>ABOUT ME</div></a>
               <a href="{{ url ('/gallery')}}"><div>GALLERY</div></a>
               <a href="{{ url ('/login')}}"><div>LOGIN</div></a>
          </div>
     </div>

     <div class="section-header">
          <div class="section-title">
               <h1>GALLERY</h1>
          </div>
     </div>

     <div class="content-section">
          <div class="pictures">
               <div><img src="../images/pic1.jpg"></div>
               <div><img src="../images/pic2.jpg"></div>
               <div><img src="../images/pic3.jpg"></div>
               <div><img src="../images/pic4.jpg"></div>
               <div><img src="../images/pic5.jpg"></div>
               <div><img src="../images/pic6.jpg"></div>
               <div><img src="../images/pic7.JPG"></div>
               <div><img src="../images/pic8.JPG"></div>
               <div><img src="../images/pic9.jpg"></div>
               <div><img src="../images/pic10.jpg"></div>
               <div><img src="../images/pic11.jpg"></div>
               <div><img src="../images/pic12.jpg"></div>
               <div><img src="../images/pic13.jpg"></div>
               <div><img src="../images/pic14.jpg"></div>
          </div>
     </div>

     <div class="container-more" id="container-more">
          <div class="container-content">
               <h1 class="title">CONTACT US!</h1>

               <div class="title-separator"></div>

               <div class="form">
                    <form>
                    <div>
                         <input type="text" class="input-form" placeholder="Name" required>
                    </div>

                    <div>
                         <input type="text" class="input-form" placeholder="Email" reuqired>
                    </div>

                    <div>
                         <textarea class="input-form" placeholder="Message..."></textarea>
                    </div>

                    <div>
                         <button class="btn-send">SEND</button>
                    </div>
                    </form>
               </div>
          </div>
     </div>


     <div class="footer">
          <div class="footer-container">
               <div>
                    <h1>DON'T MISS A BEAT!</h1>
               </div>

               <div class="sns">
                    <div><i class="fa-brands fa-facebook-f"></i></div>
                    <div><i class="fa-brands fa-instagram"></i></div>
                    <div><i class="fa-brands fa-twitter"></i></div>
                    <div><i class="fa-brands fa-linkedin-in"></i></div>
               </div>

               <div>
                    <img src="../images/logo.svg">
               </div>
          </div>
     </div>
</body>
</html>