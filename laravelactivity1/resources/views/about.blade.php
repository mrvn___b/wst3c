<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" type="text/css" href="{{ url('../css/myStyle.css') }}">
     <script src="https://kit.fontawesome.com/091cd4a75a.js" crossorigin="anonymous"></script>
     <title>MRVN | ABOUT ME</title>
</head>
<body>
     <div class="navbar">
          <div>
               <img src="../images/logo.svg" class="logo">
          </div>

          <div class="links">
               <a href="{{ url ('/home')}}"><div>HOME</div></a>
               <a href="{{ url ('/about')}}"><div>ABOUT ME</div></a>
               <a href="{{ url ('/gallery')}}"><div>GALLERY</div></a>
               <a href="{{ url ('/login')}}"><div>LOGIN</div></a>
          </div>
     </div>

     <div class="section-header">
          <div class="section-title">
               <h1>ABOUT ME</h1>
          </div>
     </div>

     <div class="content-section">
          <div class="content-details">
               <h1>Personal Details</h1>

               <table>
                    <tr>
                         <td style="width: 60%;">Name</td>
                         <td>Marvin D. Bautista</td>
                    </tr>

                    <tr>
                         <td>Age</td>
                         <td>21 years old</td>
                    </tr>

                    <tr>
                         <td>Birthdate</td>
                         <td>October 28, 2000</td>
                    </tr>

                    <tr>
                         <td>Address</td>
                         <td>San Juan, Moncada, Tarlac</td>
                    </tr>

                    <tr>
                         <td>Height</td>
                         <td>165 cm</td>
                    </tr>

                    <tr>
                         <td>Weight</td>
                         <td>54 kg</td>
                    </tr>
               </table>

               <h1>Educational Attainment</h1>

               <table>
                    <tr>
                         <td style="width: 55%;" rowspan="3">College</td>
                         <td>Pangaasinan State University</td>
                    </tr>

                    <tr>
                         <td>Urdaneta City, Pangasinan</td>
                    </tr>

                    <tr>
                         <td>2019 to present<br><br></td>
                    </tr>

                    <tr>
                         <td style="width: 55%;" rowspan="3">Senior High School</td>
                         <td>San Pedro National High School</td>
                    </tr>

                    <tr>
                         <td>Moncada, Tarlac</td>
                    </tr>

                    <tr>
                         <td>2017 to 2019<br><br></td>
                    </tr>

                    <tr>
                         <td style="width: 55%;" rowspan="3">Junior High School</td>
                         <td>San Pedro National High School</td>
                    </tr>

                    <tr>
                         <td>Moncada, Tarlac</td>
                    </tr>

                    <tr>
                         <td>201 3to 2017<br><br></td>
                    </tr>

                    <tr>
                         <td style="width: 55%;" rowspan="3">Elementary</td>
                         <td>San Juan Elementary School</td>
                    </tr>

                    <tr>
                         <td>Moncada, Tarlac</td>
                    </tr>

                    <tr>
                         <td>2006 to 2013<br><br></td>
                    </tr>
               </table>
          </div>
     </div>





     <div class="footer">
          <div class="footer-container">
               <div>
                    <h1>DON'T MISS A BEAT!</h1>
               </div>

               <div class="sns">
                    <div><i class="fa-brands fa-facebook-f"></i></div>
                    <div><i class="fa-brands fa-instagram"></i></div>
                    <div><i class="fa-brands fa-twitter"></i></div>
                    <div><i class="fa-brands fa-linkedin-in"></i></div>
               </div>

               <div>
                    <img src="../images/logo.svg">
               </div>
          </div>
     </div>
</body>
</html>