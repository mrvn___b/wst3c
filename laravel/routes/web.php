<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::Get('/customer/{customerID}/{customerName}/{customerAddress}/{customerAge?}',function($customerID, $customerName, $customerAddress, $customerAge=null){
    return "<h1>Customer</h1>
            <br> Customer ID: ".$customerID.
            "<br> Name: ".$customerName.
            "<br> Address: ".$customerAddress.
            "<br> Age: ".$customerAge.
            "<br><br>Marvin Bautista<br>BSIT-3C";
});

Route::Get('/item/{itemNo}/{itemName}/{itemPrice}',function($itemNo, $itemName, $itemPrice){
    return "<h1>Item</h1>
            <br> Item number: ".$itemNo.
            "<br> Item name: ".$itemName.
            "<br> Item price: ".$itemPrice.
            "<br><br>Marvin Bautista<br>BSIT-3C";
});

Route::Get('/order/{customerID}/{customerName}/{orderNo}/{orderDate}',function($customerID, $customerName, $orderNo, $orderDate){
    return "<h1>Order</h1>
            <br> Customer ID: ".$customerID.
            "<br> Customer name: ".$customerName.
            "<br> Order number: ".$orderNo.
            "<br> Order date: ".$orderDate.
            "<br><br>Marvin Bautista<br>BSIT-3C";
});

Route::Get('/orderDetails/{transNo}/{orderNo}/{itemID}/{itemName}/{itemPrice}/{itemQty}/{receiptNo?}',
            function($transNo, $orderNo, $itemID, $itemName, $itemPrice, $itemQty, $receiptNo=null){
                return "<h1>Order Details</h1><br> Transaction number: ".$transNo.
                        "<br> Order number: ".$orderNo.
                        "<br> Item ID: ".$itemID.
                        "<br> Item name: ".$itemName.
                        "<br> Item price: ".$itemPrice.
                        "<br> Item quantity: ".$itemQty.
                        "<br> Receipt number: ".$receiptNo.
                        "<br> Total price: ".$itemPrice * $itemQty.
                        "<br><br>Marvin Bautista<br>BSIT-3C";
            });