<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewCerticateController extends Controller
{
    protected $fpdf;
 
    public function __construct()
    {
        $this->fpdf = new Fpdf('L','mm',array(100,100));
    }

    public function view($id)
    {
        $query = DB::select('select * from certificates INNER JOIN training ON certificates.training_id = training.training_id WHERE certificates.id = "'.$id.'";');
        if($query != null)
        {
            foreach($query as $fetch)
            {
                if(isset($fetch->id)){
                    if($fetch->logo == "Template 1"){
                    $name= $fetch->name;
                    $content= $fetch->description;
                    $logo = 'assets/image/template_image/img2.png';
                    $start = date('F d, Y', strtotime($fetch->from_start_date));
                    $end = date('F d, Y', strtotime($fetch->until_end_date));
                    $organizer = $fetch->organizer;
                    $position = $fetch->position;
                    $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
        
                    $this->fpdf->SetFont('Arial', 'B', 60);
                    $this->fpdf->AddPage("L", ['500', '373']);
                    $this->fpdf->SetMargins(40,80,200,80);
                    $this->fpdf->Image($logo,0,0,0,0);
                    $this->fpdf->Image('assets/image/logo/'.$fetch->image,370,140,100);
                    
                   $this->fpdf->Ln(170); 
                    $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                    $this->fpdf->Ln(20);
                    $this->fpdf->SetFont('Arial','B',20);
                    $this->fpdf->MultiCell(0, 15,$content. ", from " . $start . " until " . $end,100,'C',0);
                    $this->fpdf->Ln(25);
                    $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                    $this->fpdf->SetFont('Arial','B',15);
                    $this->fpdf->Ln(3);
                    $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                    $this->fpdf->Ln(10);
                    $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                    $this->fpdf->Ln(15);
                    $this->fpdf->Image("$url",130,280,0,70,'PNG');
                    $this->fpdf->Ln(20); 
                    
                }
        
                else if($fetch->logo == "Template 2"){
                    $name= $fetch->name;
                    $content= $fetch->description;
                    $logo = 'assets/image/template_image/img3.png';
                    $start = date('F d, Y', strtotime($fetch->from_start_date));
                    $end = date('F d, Y', strtotime($fetch->until_end_date));
                    $organizer = $fetch->organizer;
                    $position = $fetch->position;
                    $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
        
                    $this->fpdf->SetTextColor(255, 255, 255);
        
                    $this->fpdf->SetFont('Arial', 'B', 60);
                    $this->fpdf->AddPage("L", ['500', '373']);
                    $this->fpdf->SetMargins(80,80,80,80);
                    $this->fpdf->Image($logo,0,0,0,0);
        
                    $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                    
                    $this->fpdf->Ln(170);
                    $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                    $this->fpdf->Ln(20);
                    $this->fpdf->SetFont('Arial','B',20);
                    $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                    $this->fpdf->Ln(25);
                    $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                    $this->fpdf->SetFont('Arial','B',15);
                    $this->fpdf->Ln(3);
                    $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                    $this->fpdf->Ln(10);
        
                    $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                    $this->fpdf->Image("$url",225,285,0,50,'PNG');
                    $this->fpdf->Ln(20); 
        
                    }
        
                    else if($fetch->logo == "Template 3"){
                        $name= $fetch->name;
                        $content= $fetch->description;
                        $logo = 'assets/image/template_image/img5.png';
                        $start = date('F d, Y', strtotime($fetch->from_start_date));
                        $end = date('F d, Y', strtotime($fetch->until_end_date));
                        $organizer = $fetch->organizer;
                        $position = $fetch->position;
                        $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
            
                        $this->fpdf->SetTextColor(255, 255, 255);
            
                        $this->fpdf->SetFont('Arial', 'B', 60);
                        $this->fpdf->AddPage("L", ['500', '373']);
                        $this->fpdf->SetMargins(80,80,80,80);
                        $this->fpdf->Image($logo,0,0,0,0);
            
                        $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                        
                        $this->fpdf->Ln(170);
                        $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                        $this->fpdf->Ln(20);
                        $this->fpdf->SetFont('Arial','B',20);
                        $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                        $this->fpdf->Ln(25);
                        $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                        $this->fpdf->SetFont('Arial','B',15);
                        $this->fpdf->Ln(3);
                        $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                        $this->fpdf->Ln(10);
            
                        $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                        $this->fpdf->Image("$url",225,285,0,50,'PNG');
                        $this->fpdf->Ln(20); 
            
                    }
        
                    else if($fetch->logo == "Template 4"){
                        $name= $fetch->name;
                        $content= $fetch->description;
                        $logo = 'assets/image/template_image/t1.png';
                        $start = date('F d, Y', strtotime($fetch->from_start_date));
                        $end = date('F d, Y', strtotime($fetch->until_end_date));
                        $organizer = $fetch->organizer;
                        $position = $fetch->position;
                        $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
            
                      
                        $this->fpdf->SetFont('Arial', 'B', 60);
                        $this->fpdf->AddPage("L", ['500', '373']);
                        $this->fpdf->SetMargins(80,80,80,80);
                        $this->fpdf->Image($logo,0,0,0,0);
            
                        $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                        
                        $this->fpdf->Ln(170);
                        $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                        $this->fpdf->Ln(20);
                        $this->fpdf->SetFont('Arial','B',20);
                        $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                        $this->fpdf->Ln(25);
                        $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                        $this->fpdf->SetFont('Arial','B',15);
                        $this->fpdf->Ln(3);
                        $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                        $this->fpdf->Ln(10);
            
                        $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                        $this->fpdf->Image("$url",225,285,0,50,'PNG');
                        $this->fpdf->Ln(20); 
            
                        }
        
                        else if($fetch->logo == "Template 5"){
                            $name= $fetch->name;
                            $content= $fetch->description;
                            $logo = 'assets/image/template_image/t2.png';
                            $start = date('F d, Y', strtotime($fetch->from_start_date));
                            $end = date('F d, Y', strtotime($fetch->until_end_date));
                            $organizer = $fetch->organizer;
                            $position = $fetch->position;
                            $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
                
                            
                
                            $this->fpdf->SetFont('Arial', 'B', 60);
                            $this->fpdf->AddPage("L", ['500', '373']);
                            $this->fpdf->SetMargins(80,80,80,80);
                            $this->fpdf->Image($logo,0,0,0,0);
                
                            $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                            
                            $this->fpdf->Ln(170);
                            $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                            $this->fpdf->Ln(20);
                            $this->fpdf->SetFont('Arial','B',20);
                            $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                            $this->fpdf->Ln(25);
                            $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                            $this->fpdf->SetFont('Arial','B',15);
                            $this->fpdf->Ln(3);
                            $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                            $this->fpdf->Ln(10);
                
                            $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                            $this->fpdf->Image("$url",225,285,0,50,'PNG');
                            $this->fpdf->Ln(20); 
                
                            }
        
        
                    else if($fetch->logo == "Template 6"){
                        $name= $fetch->name;
                        $content= $fetch->description;
                        $logo = 'assets/image/template_image/t3.png';
                        $start = date('F d, Y', strtotime($fetch->from_start_date));
                        $end = date('F d, Y', strtotime($fetch->until_end_date));
                        $organizer = $fetch->organizer;
                        $position = $fetch->position;
                        $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
            
                       
            
                        $this->fpdf->SetFont('Arial', 'B', 60);
                        $this->fpdf->AddPage("L", ['500', '373']);
                        $this->fpdf->SetMargins(80,80,80,80);
                        $this->fpdf->Image($logo,0,0,0,0);
            
                        $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,80);
                        
                        $this->fpdf->Ln(170);
                        $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                        $this->fpdf->Ln(20);
                        $this->fpdf->SetFont('Arial','B',20);
                        $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                        $this->fpdf->Ln(25);
                        $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                        $this->fpdf->SetFont('Arial','B',15);
                        $this->fpdf->Ln(3);
                        $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                        $this->fpdf->Ln(10);
            
                        $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                        $this->fpdf->Image("$url",225,285,0,50,'PNG');
                        $this->fpdf->Ln(20); 
            
                        }
        
                        else if($fetch->logo == "Template 7"){
                            $name= $fetch->name;
                            $content= $fetch->description;
                            $logo = 'assets/image/template_image/t4.png';
                            $start = date('F d, Y', strtotime($fetch->from_start_date));
                            $end = date('F d, Y', strtotime($fetch->until_end_date));
                            $organizer = $fetch->organizer;
                            $position = $fetch->position;
                            $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
                
                           
                
                            $this->fpdf->SetFont('Arial', 'B', 60);
                            $this->fpdf->AddPage("L", ['500', '373']);
                            $this->fpdf->SetMargins(80,80,80,80);
                            $this->fpdf->Image($logo,0,0,0,0);
                
                            $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                            
                            $this->fpdf->Ln(170);
                            $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                            $this->fpdf->Ln(20);
                            $this->fpdf->SetFont('Arial','B',20);
                            $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                            $this->fpdf->Ln(25);
                            $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                            $this->fpdf->SetFont('Arial','B',15);
                            $this->fpdf->Ln(3);
                            $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                            $this->fpdf->Ln(10);
                
                            $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                            $this->fpdf->Image("$url",225,285,0,50,'PNG');
                            $this->fpdf->Ln(20); 
                
                            }
        
                    else if($fetch->logo == "Template 8"){
                        $name= $fetch->name;
                        $content= $fetch->description;
                        $logo = 'assets/image/template_image/t5.png';
                        $start = date('F d, Y', strtotime($fetch->from_start_date));
                        $end = date('F d, Y', strtotime($fetch->until_end_date));
                        $organizer = $fetch->organizer;
                        $position = $fetch->position;
                        $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
            
                        
            
                        $this->fpdf->SetFont('Arial', 'B', 60);
                        $this->fpdf->AddPage("L", ['500', '373']);
                        $this->fpdf->SetMargins(80,80,80,80);
                        $this->fpdf->Image($logo,0,0,0,0);
            
                        $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,80);
                        
                        $this->fpdf->Ln(170);
                        $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                        $this->fpdf->Ln(20);
                        $this->fpdf->SetFont('Arial','B',20);
                        $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                        $this->fpdf->Ln(25);
                        $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                        $this->fpdf->SetFont('Arial','B',15);
                        $this->fpdf->Ln(3);
                        $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                        $this->fpdf->Ln(10);
            
                        $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                        $this->fpdf->Image("$url",225,285,0,50,'PNG');
                        $this->fpdf->Ln(20); 
            
                        }
        
                        else if($fetch->logo == "Template 9"){
                            $name= $fetch->name;
                            $content= $fetch->description;
                            $logo = 'assets/image/template_image/t6.png';
                            $start = date('F d, Y', strtotime($fetch->from_start_date));
                            $end = date('F d, Y', strtotime($fetch->until_end_date));
                            $organizer = $fetch->organizer;
                            $position = $fetch->position;
                            $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
                
                            $this->fpdf->SetTextColor(255, 255, 255);
                
                            $this->fpdf->SetFont('Arial', 'B', 60);
                            $this->fpdf->AddPage("L", ['500', '373']);
                            $this->fpdf->SetMargins(80,80,80,80);
                            $this->fpdf->Image($logo,0,0,0,0);
                
                            $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                            
                            $this->fpdf->Ln(170);
                            $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                            $this->fpdf->Ln(20);
                            $this->fpdf->SetFont('Arial','B',20);
                            $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                            $this->fpdf->Ln(25);
                            $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                            $this->fpdf->SetFont('Arial','B',15);
                            $this->fpdf->Ln(3);
                            $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                            $this->fpdf->Ln(10);
                
                            $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                            $this->fpdf->Image("$url",225,285,0,50,'PNG');
                            $this->fpdf->Ln(20); 
                
                            }
        
        
                else if($fetch->logo == "Template 10"){
                    $name= $fetch->name;
                    $content= $fetch->description;
                    $logo = 'assets/image/template_image/t7.png';
                    $start = date('F d, Y', strtotime($fetch->from_start_date));
                    $end = date('F d, Y', strtotime($fetch->until_end_date));
                    $organizer = $fetch->organizer;
                    $position = $fetch->position;
                    $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
        
                    $this->fpdf->SetTextColor(255, 255, 255);
        
                    $this->fpdf->SetFont('Arial', 'B', 60);
                    $this->fpdf->AddPage("L", ['500', '373']);
                    $this->fpdf->SetMargins(80,80,80,80);
                    $this->fpdf->Image($logo,0,0,0,0);
        
                    $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                    
                    $this->fpdf->Ln(140);
                    $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                    $this->fpdf->Ln(20);
                    $this->fpdf->SetFont('Arial','B',20);
                    $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                    $this->fpdf->Ln(25);
                    $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                    $this->fpdf->SetFont('Arial','B',15);
                    $this->fpdf->Ln(3);
                    $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                    $this->fpdf->Ln(10);
        
                    $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                    $this->fpdf->Image("$url",225,285,0,50,'PNG');
                    $this->fpdf->Ln(20); 
        
                    }
        
                else if($fetch->logo == "Template 11"){
                    $name= $fetch->name;
                    $content= $fetch->description;
                    $logo = 'assets/image/template_image/t8.png';
                    $start = date('F d, Y', strtotime($fetch->from_start_date));
                    $end = date('F d, Y', strtotime($fetch->until_end_date));
                    $organizer = $fetch->organizer;
                    $position = $fetch->position;
                    $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
        
                   
        
                    $this->fpdf->SetFont('Arial', 'B', 60);
                    $this->fpdf->AddPage("L", ['500', '373']);
                    $this->fpdf->SetMargins(80,80,80,80);
                    $this->fpdf->Image($logo,0,0,0,0);
        
                    $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                    
                    $this->fpdf->Ln(170);
                    $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                    $this->fpdf->Ln(20);
                    $this->fpdf->SetFont('Arial','B',20);
                    $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                    $this->fpdf->Ln(25);
                    $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                    $this->fpdf->SetFont('Arial','B',15);
                    $this->fpdf->Ln(3);
                    $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                    $this->fpdf->Ln(10);
        
                    $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                    $this->fpdf->Image("$url",225,285,0,50,'PNG');
                    $this->fpdf->Ln(20); 
        
                    }
        
        
                    else if($fetch->logo == "Template 12"){
                        $name= $fetch->name;
                        $content= $fetch->description;
                        $logo = 'assets/image/template_image/t9.png';
                        $start = date('F d, Y', strtotime($fetch->from_start_date));
                        $end = date('F d, Y', strtotime($fetch->until_end_date));
                        $organizer = $fetch->organizer;
                        $position = $fetch->position;
                        $url = "https://chart.googleapis.com/chart?cht=qr&chs=50x50&chl={$fetch->certificate_id}&chld=L|1&choe=UTF-8";
            
                       
                        $this->fpdf->SetFont('Arial', 'B', 60);
                        $this->fpdf->AddPage("L", ['500', '373']);
                        $this->fpdf->SetMargins(80,80,80,80);
                        $this->fpdf->Image($logo,0,0,0,0);
            
                        $this->fpdf->Image('assets/image/logo/'.$fetch->image,40,40,60);
                        
                        $this->fpdf->Ln(170);
                        $this->fpdf->Cell(0, 0, $name, 0, 0, 'C');
                        $this->fpdf->Ln(20);
                        $this->fpdf->SetFont('Arial','B',20);
                        $this->fpdf->MultiCell(0, 15,$content. ", from " . $fetch->from_start_date . " until " . $fetch->until_end_date,100,'C',0);
                        $this->fpdf->Ln(25);
                        $this->fpdf->MultiCell(0, 5,"$organizer",0,'C',0);
                        $this->fpdf->SetFont('Arial','B',15);
                        $this->fpdf->Ln(3);
                        $this->fpdf->MultiCell(0, 5,$position,0,'C',0);
                        $this->fpdf->Ln(10);
            
                        $this->fpdf->Cell(0, 0, "Certificate ID: $fetch->certificate_id", 0, 0, 'C');
                        $this->fpdf->Image("$url",225,285,0,50,'PNG');
                        $this->fpdf->Ln(20); 
            
                    }
                }
            }
        }
        return view($this->fpdf->Output('D'));
            
        exit;
    }
}
