<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
     <title>SETTINGS | ADMIN</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               background-color: #363636;
               position: fixed;
               width: 100%;
               z-index: 1;
          }

          .nav > div {
               padding: 15px;
          }

          .logo {
               height: 30px;
          }

          .m-image {
               width: 150px;
               height: 150px;
               border-radius: 75px;
               margin-top: 20px;
               object-fit: cover;
          }

          .section {
               padding-top: 65px;
               background-color: white;
               height: 100vh;
               width: 20%;
               box-shadow: 10px 0 10px -10px rgba(0,0,0,0.2), -5px 0 5px -5px rgba(0,0,0,0.4);
               position: fixed;
               z-index: -1;
          }

          .active {
               background-color: #DCDCDC;
               border-radius: 0 25px 25px 0;
          }

          .linkword {
               text-decoration: none;
               color: #363636;
          }

          .link {
               font-size: 18px;
               padding: 10px;
          }

          .link:hover {
               background-color: #f0eded;
               border-radius: 0 25px 25px 0;
               color: #363636;
          }

          .content {
               float: right;
               padding-top: 115px;
               padding-left: 70px;
               padding-right: 120px;
               margin-left: 20%;
               width: 80%;
          }

          .banner-container {
               padding: 50px;
          }

          .desc {
               height: 100px;
               text-overflow: ellipsis;
               resize: none;
          }

          .btn-submit {
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 150px;
               height: 45px;
          }

          .btn-submit:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-delete {
               background-color: #DC3545;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-delete:hover {
               background-color: #AB1625;
               color: #ffffff;
          }

          .btn-add {
               margin-left: 10px;
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-add:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-edit {
               background-color: #165FFA;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-edit:hover {
               background-color: #0753F5;
               color: #ffffff;
          }

          p {
               margin: 0;
          }

          .new_image {
               height: 150px;
               width: 150px;
               border-radius: 75px;
               margin-right: 20px;
               background-size: cover;
          }

          span {
               margin-right: 5px;
          }

          .empty {
               height: 400px;
          }

          .empty_note {
               font-size: 20px;
          }

          span {
               margin-right: 5px;
               vertical-align: middle;
          }

          .admin {
               color: #ffffff;
               margin-left: 20px;
               border-left: 1px solid #ffffff;
               padding-left: 20px;
          }

          .logout {
               color: #ffffff;
               text-decoration: none;
          }

          .logout:hover {
               color: #ffffff;
               text-decoration: underline;
          }

          .btn-short {
               background-color: #6F8762;
               color: #ffffff;
               border-radius: 25px;
               width: 120px;
          }

          .btn-short:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-logo {
               margin-top: 10px;
               width: 100%;
               background-color: #6F8762;
               border-radius: 25px;
               color: #ffffff;
          }

          .btn-logo:hover {
               background-color: #495C40;
               color: #ffffff;
          }
     </style>
</head>
<body>
     <div class="d-flex justify-content-between align-items-center nav">
          <div class="d-flex align-items-center">
               <div>
                    @foreach($settings as $s)
                    <img class="logo" src="{{ url ('../images/' . $s->header_logo) }}">
                    @endforeach
               </div>

               <div class="admin">
                    <h3 class="m-0">Administrator</h3>
               </div>
          </div>

          <div class="d-flex">
               <a href="{{url ('/admin/login')}}" class="logout"><span class="material-symbols-rounded">logout</span> Logout</a>
          </div>
     </div>

     <div class="section">
          <a class="linkword" href="{{url ('/admin/home')}}">
               <div class="link">
                    <span class="material-symbols-outlined">home</span>
                    Home
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/new')}}">
               <div class="link">
                    <span class="material-symbols-outlined">diamond</span>
                    New
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/about')}}">
               <div class="link">
                    <span class="material-symbols-outlined">info</span>
                    About
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/faqs')}}">
               <div class="link">
                    <span class="material-symbols-outlined">help</span>
                    FAQs
               </div> 
          </a>

          <a class="linkword" href="{{url ('/admin/settings')}}">
               <div class="link active">
                    <span class="material-symbols-outlined">settings</span>
                    Settings
               </div> 
          </a>
     </div>

          <div class="content">
                    <div class="card w-75 shadow">
                         <div class="card-header">
                              <h5 class="card-title">Administrator Credentials</h5>
                         </div>
                         
                         <div class="card-body">
                              @foreach($admin as $a)
                                   <div class="row">
                                        <div class="col-sm-4">
                                             <label class="form-label">Username</label>
                                             <input type="text" class="form-control" value="{{ $a->username }}" readonly>
                                        </div>
                                   </div>

                                   <div class="row mt-3">
                                        <div class="col-sm-4">
                                             <label class="form-label">Password</label>
                                             <input type="password" class="form-control" value="{{ $a->password }}" readonly>
                                        </div>
                                   </div>

                                   <div class="d-flex justify-content-end mt-3">
                                        <div>
                                             <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#adminModal">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                            </div>

                                                            <div>
                                                                 Update
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                                   </div>
                              @endforeach
                         </div>
                    </div>

                    <div class="modal fade" id="adminModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                         <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Update Admin Credentials</h5>
                                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>

                              <div class="modal-body">
                                   <form action="/update-admin" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                             <div class="form-group pt-2">
                                                  <label for="new_name" class="form-label">New username</label>
                                                  <input type="text" class="form-control" name="username" required>
                                             </div>

                                             <div class="form-group pt-2">
                                                  <label for="new_name" class="form-label">New password</label>
                                                  <input type="password" class="form-control" name="password" required>
                                             </div>

                                             <div class="form-group pt-2 mt-3">
                                                  <div class="d-flex justify-content-end">
                                                       <div>
                                                            <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">cancel</span>
                                                                           </div>

                                                                           <div>
                                                                                Cancel
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>

                                                       <div>
                                                            <button class="btn btn-add" type="submit">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">edit_note</span>
                                                                           </div>

                                                                           <div>
                                                                                Update
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                   </form>
                              </div>
                              </div>
                         </div>
                    </div>

                    <div class="card w-75 shadow mt-5">
                         <div class="card-header">
                              <h5 class="card-title">Email</h5>
                         </div>
                         
                         <div class="card-body">
                              @foreach($settings as $s)
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">mail</span>
                                        </div>

                                        <div>
                                             <p class="card-text">{{ $s->email }}</p>
                                        </div>
                                   </div>

                                   <div class="d-flex justify-content-end mt-3">
                                        <div>
                                             <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#emailModal">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                            </div>

                                                            <div>
                                                                 Update
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                                   </div>
                              @endforeach
                         </div>
                    </div>

                    <div class="modal fade" id="emailModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                         <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Update Email</h5>
                                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>

                              <div class="modal-body">
                                   <form action="/update-email" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                             <div class="form-group pt-2">
                                             <label for="new_name" class="form-label">Email</label>
                                             <input type="email" class="form-control" name="email" required>
                                             </div>

                                             <div class="form-group pt-2 mt-3">
                                                  <div class="d-flex justify-content-end">
                                                       <div>
                                                            <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">cancel</span>
                                                                           </div>

                                                                           <div>
                                                                                Cancel
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>

                                                       <div>
                                                            <button class="btn btn-add" type="submit">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">edit_note</span>
                                                                           </div>

                                                                           <div>
                                                                                Update
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                   </form>
                              </div>
                              </div>
                         </div>
                    </div>

                    <div class="card w-75 shadow mt-5">
                         <div class="card-header">
                              <h5 class="card-title">Facebook Page</h5>
                         </div>
                         
                         <div class="card-body">
                              @foreach($settings as $s)
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">link</span>
                                        </div>

                                        <div>
                                             <p class="card-text">{{ $s->fb_link }}</p>
                                        </div>
                                   </div>

                                   <div class="d-flex justify-content-end mt-3">
                                        <div style="margin-right: 10px;">
                                             <a class="btn btn-short shadow mt-3" href="{{ $s->fb_link }}" target="_blank">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">tour</span>
                                                            </div>

                                                            <div>
                                                                 View
                                                            </div>
                                                       </div>
                                                  </div>
                                             </a>
                                        </div>

                                        <div>
                                             <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#fbModal">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                            </div>

                                                            <div>
                                                                 Update
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                                   </div>
                              @endforeach
                         </div>
                    </div>

                    <div class="modal fade" id="fbModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                         <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Update Facebook Page</h5>
                                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>

                              <div class="modal-body">
                                   <form action="/update-fb" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                             <div class="form-group pt-2">
                                             <label for="new_name" class="form-label">Link</label>
                                             <input type="text" class="form-control" name="fb_link" required>
                                             </div>

                                             <div class="form-group pt-2 mt-3">
                                                  <div class="d-flex justify-content-end">
                                                       <div>
                                                            <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">cancel</span>
                                                                           </div>

                                                                           <div>
                                                                                Cancel
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>

                                                       <div>
                                                            <button class="btn btn-add" type="submit">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">edit_note</span>
                                                                           </div>

                                                                           <div>
                                                                                Update
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                   </form>
                              </div>
                              </div>
                         </div>
                    </div>

                    <div class="card w-75 shadow mt-5">
                         <div class="card-header">
                              <h5 class="card-title">Shopee Store</h5>
                         </div>
                         
                         <div class="card-body">
                              @foreach($settings as $s)
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">link</span>
                                        </div>

                                        <div>
                                             <p class="card-text">{{ $s->shopee_link }}</p>
                                        </div>
                                   </div>

                                   <div class="d-flex justify-content-end mt-3">
                                        <div style="margin-right: 10px;">
                                             <a class="btn btn-short shadow mt-3" href="{{ $s->shopee_link }}" target="_blank">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">tour</span>
                                                            </div>

                                                            <div>
                                                                 View
                                                            </div>
                                                       </div>
                                                  </div>
                                             </a>
                                        </div>

                                        <div>
                                             <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#shopeeModal">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                            </div>

                                                            <div>
                                                                 Update
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                                   </div>
                              @endforeach
                         </div>
                    </div>

                    <div class="modal fade" id="shopeeModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                         <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Update Shopee Store</h5>
                                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>

                              <div class="modal-body">
                                   <form action="/update-shopee" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                             <div class="form-group pt-2">
                                             <label for="new_name" class="form-label">Link</label>
                                             <input type="text" class="form-control" name="shopee_link" required>
                                             </div>

                                             <div class="form-group pt-2 mt-3">
                                                  <div class="d-flex justify-content-end">
                                                       <div>
                                                            <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">cancel</span>
                                                                           </div>

                                                                           <div>
                                                                                Cancel
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>

                                                       <div>
                                                            <button class="btn btn-add" type="submit">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">edit_note</span>
                                                                           </div>

                                                                           <div>
                                                                                Update
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                   </form>
                              </div>
                              </div>
                         </div>
                    </div>

                    <div class="card w-75 shadow mt-5">
                         <div class="card-header">
                              <h5 class="card-title">Phone Number</h5>
                         </div>
                         
                         <div class="card-body">
                              @foreach($settings as $s)
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">phone</span>
                                        </div>

                                        <div>
                                             <p class="card-text">{{ $s->phone_number }}</p>
                                        </div>
                                   </div>

                                   <div class="d-flex justify-content-end mt-3">
                                        <div>
                                             <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#phoneModal">
                                                  <div class="d-flex align-items-center justify-content-center mt-1">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                            </div>

                                                            <div>
                                                                 Update
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                                   </div>
                              @endforeach
                         </div>
                    </div>

                    <div class="modal fade" id="phoneModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                         <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Update Phone</h5>
                                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>

                              <div class="modal-body">
                                   <form action="/update-phone" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                             <div class="form-group pt-2">
                                             <label for="new_name" class="form-label">Phone</label>
                                             <input type="phone" class="form-control" name="phone" required>
                                             </div>

                                             <div class="form-group pt-2 mt-3">
                                                  <div class="d-flex justify-content-end">
                                                       <div>
                                                            <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">cancel</span>
                                                                           </div>

                                                                           <div>
                                                                                Cancel
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>

                                                       <div>
                                                            <button class="btn btn-add" type="submit">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                           <span class="material-symbols-outlined">edit_note</span>
                                                                           </div>

                                                                           <div>
                                                                                Update
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                                  </div>
                                             </div>
                                   </form>
                              </div>
                              </div>
                         </div>
                    </div>

                    <div class="d-flex justify-content-start flex-wrap mt-5 mb-5">
                         @foreach($settings as $s)
                              <div class="card shadow" style="width: 18rem; margin-right: 20px">
                                   <img src="{{ url('../images/' . $s->header_logo)}}" class="card-img-top" style="height: 200px; object-fit: contain;">
                                   <div class="card-body">
                                        <h5 class="card-title mt-3">Header Logo</h5>
                                        <button class="btn btn-logo" data-bs-toggle="modal" data-bs-target="#headerModal">Update</button>
                                   </div>
                              </div>
                         @endforeach

                         <div class="modal fade" id="headerModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                              <div class="modal-dialog">
                                   <div class="modal-content">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Update Header Logo</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                   </div>

                                   <div class="modal-body">
                                        <form action="/update-header" method="post" enctype="multipart/form-data">
                                             @csrf
                                             
                                                  <div class="form-group pt-2">
                                                  <label for="new_name" class="form-label">Image</label>
                                                  <input type="file" class="form-control" accept="image/*" name="header" required>
                                                  </div>

                                                  <div class="form-group pt-2 mt-3">
                                                       <div class="d-flex justify-content-end">
                                                            <div>
                                                                 <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                      <div class="d-flex align-items-center justify-content-center">
                                                                           <div class="d-flex align-items-start">
                                                                                <div>
                                                                                <span class="material-symbols-outlined">cancel</span>
                                                                                </div>

                                                                                <div>
                                                                                     Cancel
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </button>
                                                            </div>

                                                            <div>
                                                                 <button class="btn btn-add" type="submit">
                                                                      <div class="d-flex align-items-center justify-content-center">
                                                                           <div class="d-flex align-items-start">
                                                                                <div>
                                                                                <span class="material-symbols-outlined">edit_note</span>
                                                                                </div>

                                                                                <div>
                                                                                     Update
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </button>
                                                            </div>
                                                       </div>
                                                  </div>
                                        </form>
                                   </div>
                                   </div>
                              </div>
                         </div>

                         @foreach($settings as $s)
                              <div class="card shadow" style="width: 18rem;">
                                   <img src="{{ url('../images/' . $s->footer_logo)}}" class="card-img-top" style="height: 200px; object-fit: contain">
                                   <div class="card-body">
                                        <h5 class="card-title mt-3">Footer Logo</h5>
                                        <button class="btn btn-logo" data-bs-toggle="modal" data-bs-target="#footerModal">Update</button>
                                   </div>
                              </div>
                         @endforeach

                         <div class="modal fade" id="footerModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                              <div class="modal-dialog">
                                   <div class="modal-content">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Update Footer Logo</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                   </div>

                                   <div class="modal-body">
                                        <form action="/update-footer" method="post" enctype="multipart/form-data">
                                             @csrf
                                             
                                                  <div class="form-group pt-2">
                                                  <label for="new_name" class="form-label">Image</label>
                                                  <input type="file" class="form-control" accept="image/*" name="footer" required>
                                                  </div>

                                                  <div class="form-group pt-2 mt-3">
                                                       <div class="d-flex justify-content-end">
                                                            <div>
                                                                 <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                                      <div class="d-flex align-items-center justify-content-center">
                                                                           <div class="d-flex align-items-start">
                                                                                <div>
                                                                                <span class="material-symbols-outlined">cancel</span>
                                                                                </div>

                                                                                <div>
                                                                                     Cancel
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </button>
                                                            </div>

                                                            <div>
                                                                 <button class="btn btn-add" type="submit">
                                                                      <div class="d-flex align-items-center justify-content-center">
                                                                           <div class="d-flex align-items-start">
                                                                                <div>
                                                                                <span class="material-symbols-outlined">edit_note</span>
                                                                                </div>

                                                                                <div>
                                                                                     Update
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </button>
                                                            </div>
                                                       </div>
                                                  </div>
                                        </form>
                                   </div>
                                   </div>
                              </div>
                         </div>
                    </div>
          </div>

          <script>
                    @if(Session::has('success'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false,
                              
                         }
  		               toastr.success("{{ session('success') }}");
                    @endif

                    @if(Session::has('deleted'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false
                         }
  		               toastr.error("{{ session('deleted') }}");
                    @endif
               </script>
</body>
</html>