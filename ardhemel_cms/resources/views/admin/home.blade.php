<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
     <title>HOME | ADMIN</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               background-color: #363636;
               position: fixed;
               width: 100%;
          }

          .nav > div {
               padding: 15px;
          }

          .logo {
               height: 30px;
          }

          .material-symbols-rounded {
               color: white;
               font-size: 30px;
               font-variation-settings:
                    'FILL' 1,
                    'wght' 400,
                    'GRAD' 0,
                    'opsz' 48
          }

          .section {
               padding-top: 65px;
               background-color: white;
               height: 100vh;
               width: 20%;
               box-shadow: 10px 0 10px -10px rgba(0,0,0,0.2), -5px 0 5px -5px rgba(0,0,0,0.4);
               position: fixed;
               z-index: -1;
          }

          .active {
               background-color: #DCDCDC;
               border-radius: 0 25px 25px 0;
          }

          .linkword {
               text-decoration: none;
               color: #363636;
          }

          .link {
               font-size: 18px;
               padding: 10px;

          }

          .link:hover {
               background-color: #f0eded;
               border-radius: 0 25px 25px 0;
               color: #363636;
          }

          .content {
               float: right;
               padding-top: 115px;
               padding-left: 70px;
               padding-right: 120px;
               margin-left: 20%;
               width: 80%;
          }

          .banner-container {
               padding: 50px;
          }

          .desc {
               height: 150px;
               text-overflow: ellipsis;
               resize: none;
          }

          .btn-submit {
               background-color: #6F8762;
               border-radius: 25px;
               padding: 5px 20px;
               color: white;
          }

          .btn-submit:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .section2 {
               margin-top: 40px;
          }

          span {
               margin-right: 5px;
               vertical-align: middle;
          }

          .admin {
               color: #ffffff;
               margin-left: 20px;
               border-left: 1px solid #ffffff;
               padding-left: 20px;
          }

          .banner-bg {
               background-size: cover;
               height: 500px;
               width: 100%;
          }
          .gradient-bg {
               height: 500px;
               width: 100%;
               padding-left: 70px;
               color: #363636;
               background-image: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0), rgba(255,255,255,5));
               border: 1px solid #6F8762;
          }

          .banner-title {
               font-size: 50px;
               font-weight: bold;
          }

          .banner-content {
               font-size: 14px;
               width: 500px;
               text-shadow: 2px 2px 5px white;
          }

          .logout {
               color: #ffffff;
               text-decoration: none;
          }

          .logout:hover {
               color: #ffffff;
               text-decoration: underline;
          }
     </style>
</head>
<body>
     <div class="d-flex justify-content-between align-items-center nav">
          <div class="d-flex align-items-center">
               <div>
                    @foreach($settings as $s)
                    <img class="logo" src="{{ url ('../images/' . $s->header_logo) }}">
                    @endforeach
               </div>

               <div class="admin">
                    <h3 class="m-0">Administrator</h3>
               </div>
          </div>

          <div class="d-flex">
               <a href="{{url ('/admin/login')}}" class="logout"><span class="material-symbols-rounded">logout</span> Logout</a>
          </div>
     </div>

     <div class="section">
          <a class="linkword" href="{{url ('/admin/home')}}">
               <div class="link active">
                    <span class="material-symbols-outlined">home</span>
                    Home
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/new')}}">
               <div class="link">
                    <span class="material-symbols-outlined">diamond</span>
                    New
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/about')}}">
               <div class="link">
                    <span class="material-symbols-outlined">info</span>
                    About
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/faqs')}}">
               <div class="link">
                    <span class="material-symbols-outlined">help</span>
                    FAQs
               </div> 
          </a>

          <a class="linkword" href="{{url ('/admin/settings')}}">
               <div class="link">
                    <span class="material-symbols-outlined">settings</span>
                    Settings
               </div> 
          </a>
     </div>

     <div class="content">
          <div class="d-flex justify-content-between align-items-center">
               <div>
                    <h2>Banner</h2>
               </div>

               <div>
                    <button class="btn btn-submit shadow" data-bs-toggle="modal"data-bs-target="#editbannerModal">
                         <div class="d-flex align-items-center justify-content-center mt-1">
                              <div class="d-flex align-items-start">
                                   <div>
                                        <span class="material-symbols-outlined">edit_note</span>
                                   </div>

                                   <div>
                                        Edit
                                   </div>
                              </div>
                         </div>
                    </button>
               </div>
          </div>

          <div class="col-sm-12 mt-5">
               <script>
                    @if(Session::has('success'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false,
                              
                         }
  		               toastr.success("{{ session('success') }}");
                    @endif

                    @if(Session::has('deleted'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false
                         }
  		               toastr.error("{{ session('deleted') }}");
                    @endif
               </script>

               @foreach($banner as $b)
                    <?php
                         $pic = $b->banner_pic;
                         $color = $b->banner_color;
                    ?>

                    <div class="banner-bg" style="background-image: url('/images/<?php echo $pic; ?>')">
                         <div class="gradient-bg d-flex flex-column align-items-start justify-content-center">
                              <div>
                                   <h1 class="banner-title" style="color: <?php echo $color; ?>">{{ $b->banner_title }}</h1>
                              </div>

                              <div class="mt-4 banner-content">
                                   <p class="banner-content" style="color: <?php echo $color; ?>">{{ $b->banner_desc }}</p>
                              </div>

                              <div>
                                   <button class="btn btn-submit shadow mt-3"><span class="material-symbols-outlined">expand_more</span>CATCH UP</button>
                              </div>
                         </div>
                    </div>
               @endforeach
          </div>
     </div>

          <div class="modal fade" id="editbannerModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
               <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Banner</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                              <form action="/banner-upload" method="POST" enctype="multipart/form-data">
                                   @csrf

                                   <div class="mt-3">
                                        <div class="col-sm-12">
                                             <label for="banner-pic" class="form-label">Cover</label>
                                             <input type="file" class="form-control" name="banner_image">
                                        </div>
                                   </div>

                                   <div class="row mt-3">
                                        <div class="col-sm-8">
                                             <label for="banner-pic" class="form-label">Title</label>
                                             <input type="text" class="form-control" name="banner_title">
                                        </div>

                                        <div class="col-sm-4">
                                             <label for="banner-pic" class="form-label">Text color</label>
                                             <input type="color" class="form-control form-control-color" name="banner_color">
                                        </div>
                                   </div>
          
                                   <div class="mt-3">
                                        <div class="col-sm-12">
                                             <label for="banner-pic" class="form-label">Description</label>
                                             <textarea class="form-control desc" name="banner_desc"></textarea>
                                        </div>
                                   </div>

                                   <div class="mt-3">
                                        <div class="col-sm-12">
                                             <input type="submit" class="btn btn-submit mb-5" name="submit" value="UPDATE">
                                        </div>
                                   </div>
                              </form>
                         </div>
                    </div>
               </div>
          </div>
</body>
</html>