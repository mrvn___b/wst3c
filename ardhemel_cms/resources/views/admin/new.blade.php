<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
     <title>NEW | ADMIN</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               background-color: #363636;
               position: fixed;
               width: 100%;
               z-index: 1;
          }

          .nav > div {
               padding: 15px;
          }

          .logo {
               height: 30px;
          }

          .material-symbols-rounded {
               color: white;
               font-size: 30px;
               font-variation-settings:
                    'FILL' 1,
                    'wght' 400,
                    'GRAD' 0,
                    'opsz' 48
          }

          .section {
               padding-top: 65px;
               background-color: white;
               height: 100vh;
               width: 20%;
               box-shadow: 10px 0 10px -10px rgba(0,0,0,0.2), -5px 0 5px -5px rgba(0,0,0,0.4);
               position: fixed;
               z-index: -1;
          }

          .active {
               background-color: #DCDCDC;
               border-radius: 0 25px 25px 0;
          }

          .linkword {
               text-decoration: none;
               color: #363636;
          }

          .link {
               font-size: 18px;
               padding: 10px;
          }

          .link:hover {
               background-color: #f0eded;
               border-radius: 0 25px 25px 0;
               color: #363636;
          }

          .content {
               float: right;
               padding-top: 115px;
               padding-left: 70px;
               padding-right: 120px;
               margin-left: 20%;
               width: 80%;
          }

          .banner-container {
               padding: 50px;
          }

          .desc {
               height: 100px;
               text-overflow: ellipsis;
               resize: none;
          }

          .btn-submit {
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 150px;
               height: 45px;
          }

          .btn-submit:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-delete {
               background-color: #DC3545;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-show {
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-show:hover {
               background-color: #495C40;
               color: #FFFFFF;
          }


          .btn-delete:hover {
               background-color: #AB1625;
               color: #ffffff;
          }

          .btn-add {
               margin-left: 10px;
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-add:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-edit {
               background-color: #165FFA;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-edit:hover {
               background-color: #0753F5;
               color: #ffffff;
          }

          p {
               margin: 0;
          }

          .new_image {
               height: 320px;
               width: 250px;
               border-radius: 50px;
               margin-right: 30px;
               object-fit: cover;
          }

          span {
               margin-right: 5px;
          }

          .empty {
               height: 300px;
          }

          .empty_note {
               margin-top: 20px;
               font-size: 16px;
          }

          span {
               margin-right: 5px;
               vertical-align: middle;
          }

          .admin {
               color: #ffffff;
               margin-left: 20px;
               border-left: 1px solid #ffffff;
               padding-left: 20px;
          }

          .logout {
               color: #ffffff;
               text-decoration: none;
          }

          .logout:hover {
               color: #ffffff;
               text-decoration: underline;
          }
     </style>
</head>
<body>
     <div class="d-flex justify-content-between align-items-center nav">
          <div class="d-flex align-items-center">
               <div>
                    @foreach($settings as $s)
                    <img class="logo" src="{{ url ('../images/' . $s->header_logo) }}">
                    @endforeach
               </div>

               <div class="admin">
                    <h3 class="m-0">Administrator</h3>
               </div>
          </div>

          <div class="d-flex">
               <a href="{{url ('/admin/login')}}" class="logout"><span class="material-symbols-rounded">logout</span> Logout</a>
          </div>
     </div>

     <div class="section">
          <a class="linkword" href="{{url ('/admin/home')}}">
               <div class="link">
                    <span class="material-symbols-outlined">home</span>
                    Home
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/new')}}">
               <div class="link active">
                    <span class="material-symbols-outlined">diamond</span>
                    New
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/about')}}">
               <div class="link">
                    <span class="material-symbols-outlined">info</span>
                    About
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/faqs')}}">
               <div class="link">
                    <span class="material-symbols-outlined">help</span>
                    FAQs
               </div> 
          </a>

          <a class="linkword" href="{{url ('/admin/settings')}}">
               <div class="link">
                    <span class="material-symbols-outlined">settings</span>
                    Settings
               </div> 
          </a>
     </div>

     <div class="content">
          <div class="col-sm-12">
               <script>
                    @if(Session::has('success'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false,
                              
                         }
  		               toastr.success("{{ session('success') }}");
                    @endif

                    @if(Session::has('deleted'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false
                         }
  		               toastr.error("{{ session('deleted') }}");
                    @endif
               </script>

               <div class="d-flex justify-content-between align-items-center">
                    <div>
                         <h2>What's New?</h2>
                    </div>

                    <div>
                         <button class="btn btn-submit shadow" data-bs-toggle="modal" data-bs-target="#addModal">
                              <div class="d-flex align-items-center justify-content-center mt-1">
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">add_circle</span>
                                        </div>

                                        <div>
                                             New Post
                                        </div>
                                   </div>
                              </div>
                         </button>
                    </div>
               </div>

               <div class="col-sm-12 mt-5">
                    @foreach($new as $n)
                         <div class="card w-75 shadow mb-3" style="width: 18rem;">
                              <div class="card-body">
                                   <div class="d-flex align-items-center">
                                        <div>
                                             <img src="{{ url('/images/' . $n->new_image) }}" class="new_image shadow">
                                        </div>

                                        <div class="d-flex flex-column">
                                             <h5 class="card-title">{{$n->new_item}}</h5>

                                             <div class="d-flex align-items-start mb-3">
                                                  <div>
                                                       <span class="material-symbols-outlined">payments</span>
                                                  </div>

                                                  <div>
                                                       <h5>₱{{$n->new_price}}</h5>
                                                  </div>
                                             </div>

                                             <div class="d-flex align-items-start">
                                                  <div>
                                                       <span class="material-symbols-outlined">description</span>
                                                  </div>

                                                  <div>
                                                       {{$n->new_desc}}
                                                  </div>
                                             </div>

                                             <div class="d-flex align-items-start">
                                                  <div>
                                                       <span class="material-symbols-outlined">link</span>
                                                  </div>

                                                  <div>
                                                       {{$n->new_link}}
                                                  </div>
                                             </div>
                                        </div>
                                   </div>

                                   @if( $n->new_status == 0)
                                        <div class="d-flex justify-content-end mt-3">
                                             <form action="/hide-new" method="POST">
                                                  @csrf

                                                  <input type="hidden" name="new_id" value="{{$n->new_id}}">
                                                  
                                                       <div style="margin-left: 10px;">
                                                            <button type="submit" class="btn btn-delete" onclick="return confirm('Are you sure you want to hide this item?');">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                                <span class="material-symbols-outlined">visibility_off</span>
                                                                           </div>

                                                                           <div>
                                                                                Hide
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                             </form>
                                        </div>
                                   @endif

                                   @if( $n->new_status == 1)
                                        <div class="d-flex justify-content-end mt-3">
                                             <form action="/show-new" method="POST">
                                                  @csrf

                                                  <input type="hidden" name="new_id" value="{{$n->new_id}}">
                                                  
                                                       <div style="margin-left: 10px;">
                                                            <button type="submit" class="btn btn-show" onclick="return confirm('Are you sure you want to show this item?');">
                                                                 <div class="d-flex align-items-center justify-content-center">
                                                                      <div class="d-flex align-items-start">
                                                                           <div>
                                                                                <span class="material-symbols-outlined">visibility</span>
                                                                           </div>

                                                                           <div>
                                                                                Show
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </button>
                                                       </div>
                                             </form>
                                        </div>
                                   @endif
                              </div>
                         </div>
                    @endforeach

                    @if ($new->isEmpty())
                    <div class="d-flex flex-column align-items-center">
                         <div>
                              <img src="{{ url('../images/empty.png')}}" class="empty">
                         </div>

                         <div>
                              <p class="empty_note">You don't have any items. Add new!</p>
                         </div>
                    </div>
                    @endif
               </div>
          </div>
     </div>

     <div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Item</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <form action="/add-new" method="post" enctype="multipart/form-data">
                                 @csrf
                                 
                                   <div class="form-group pt-2">
                                      <label for="new_name" class="form-label">Item name</label>
                                      <input type="text" class="form-control" name="new_name" required>
                                   </div>

                                   <div class="form-group pt-2">
                                        <div class="row">
                                             <div class="col-sm-12">
                                                  <label for="new_name" class="form-label">Description</label>
                                                  <textarea class="form-control" name="new_desc" required></textarea>
                                             </div>
                                        </div>
                                   </div>

                                   <div class="form-group pt-2">
                                        <div class="row">
                                             <div class="col-sm-12">
                                                  <label for="new_name" class="form-label">Image</label>
                                                  <input type="file" class="form-control" name="new_image" accept="image/*" required>
                                             </div>
                                        </div>
                                   </div>

                                   <div class="form-group pt-2">
                                        <div class="row">
                                             <div class="col-sm-8">
                                                  <label for="new_name" class="form-label">Link</label>
                                                  <input type="text" class="form-control" name="new_link" required>
                                             </div>

                                             <div class="col-sm-4">
                                                  <label for="new_name" class="form-label">Price</label>
                                                  <input type="number" class="form-control" name="new_price" required>
                                             </div>
                                        </div>
                                   </div>

                                   <div class="form-group pt-2 mt-3">
                                        <div class="d-flex justify-content-end">
                                             <div>
                                                  <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                       <div class="d-flex align-items-center justify-content-center">
                                                            <div class="d-flex align-items-start">
                                                                 <div>
                                                                 <span class="material-symbols-outlined">cancel</span>
                                                                 </div>

                                                                 <div>
                                                                      Cancel
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </button>
                                             </div>

                                             <div>
                                                  <button class="btn btn-add" type="submit">
                                                       <div class="d-flex align-items-center justify-content-center">
                                                            <div class="d-flex align-items-start">
                                                                 <div>
                                                                 <span class="material-symbols-outlined">add_circle</span>
                                                                 </div>

                                                                 <div>
                                                                      Add
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </button>
                                             </div>
                                        </div>
                                   </div>
                            </form>
                        </div>
                    </div>
                </div>
      </div>
</body>
</html>