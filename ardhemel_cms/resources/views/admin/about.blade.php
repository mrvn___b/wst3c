<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
     <title>ABOUT | ADMIN</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               background-color: #363636;
               position: fixed;
               width: 100%;
               z-index: 1;
          }

          .nav > div {
               padding: 15px;
          }

          .logo {
               height: 30px;
          }

          .material-symbols-rounded {
               color: white;
               font-size: 30px;
               font-variation-settings:
                    'FILL' 1,
                    'wght' 400,
                    'GRAD' 0,
                    'opsz' 48
          }

          .section {
               padding-top: 65px;
               background-color: white;
               height: 100vh;
               width: 20%;
               box-shadow: 10px 0 10px -10px rgba(0,0,0,0.2), -5px 0 5px -5px rgba(0,0,0,0.4);
               position: fixed;
               z-index: -1;
          }

          .active {
               background-color: #DCDCDC;
               border-radius: 0 25px 25px 0;
          }

          .linkword {
               text-decoration: none;
               color: #363636;
          }

          .link {
               font-size: 18px;
               padding: 10px;
          }

          .link:hover {
               background-color: #f0eded;
               border-radius: 0 25px 25px 0;
               color: #363636;
          }

          .content {
               float: right;
               padding-top: 115px;
               padding-left: 70px;
               padding-right: 120px;
               margin-left: 20%;
               width: 80%;
          }

          .banner-container {
               padding: 50px;
          }

          .desc {
               height: 150px;
               text-overflow: ellipsis;
               resize: none;
          }

          .btn-submit {
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 150px;
               height: 45px;
          }

          .btn-submit:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-delete {
               background-color: #DC3545;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-delete:hover {
               background-color: #AB1625;
               color: #ffffff;
          }

          .btn-add {
               margin-left: 10px;
               background-color: #6F8762;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-add:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-edit {
               background-color: #165FFA;
               border-radius: 25px;
               color: white;
               width: 110px;
               height: 40px;
          }

          .btn-edit:hover {
               background-color: #0753F5;
               color: #ffffff;
          }

          .section2 {
               margin-top: 40px;
          }

          .empty {
               height: 300px;
          }

          .empty_note {
               margin-top: 20px;
               font-size: 16px;
          }

          span {
               margin-right: 5px;
               vertical-align: middle;
          }

          .m {
               margin-top: 25px;
               margin-right: 25px;
          }

          .m-image {
               width: 150px;
               height: 150px;
               border-radius: 75px;
               margin-top: 20px;
               object-fit: cover;
          }

          .btn-fb {
               width: 80%;
               background-color: #6F8762;
               border-radius: 25px;
               color: #ffffff;
          }

          .btn-fb:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .delete-short {
               background-color: #ffffff;
               margin-right: 10px;
               border-radius: 20px;
               width: 40px;
               height: 40px;
               color: #363636;
               border: none;
               text-align: center;
          }

          .delete-short:hover {
               border-radius: 20px;
               background-color: #f0f0f0;
               width: 40px;
               height: 40px;
               color: #363636;
               border: none;
               text-align: center;
          }

          p {
               margin: 0;
          }

          .edit-short {
               background-color: #ffffff;
               border-radius: 20px;
               width: 40px;
               height: 40px;
               color: #363636;
               border: none;
               text-align: center;
          }

          .edit-short:hover {
               border-radius: 20px;
               background-color: #f0f0f0;
               width: 40px;
               height: 40px;
               color: #363636;
               border: none;
               text-align: center;
          }

          .admin {
               color: #ffffff;
               margin-left: 20px;
               border-left: 1px solid #ffffff;
               padding-left: 20px;
          }

          .btn-short {
               background-color: #6F8762;
               color: #ffffff;
               border-radius: 25px;
               width: 120px;
          }

          .btn-short:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .about-text {
               resize: none;
               height: 300px;
          }

          .logout {
               color: #ffffff;
               text-decoration: none;
          }

          .logout:hover {
               color: #ffffff;
               text-decoration: underline;
          }
     </style>
</head>
<body>
     <div class="d-flex justify-content-between align-items-center nav">
          <div class="d-flex align-items-center">
               <div>
                    @foreach($settings as $s)
                    <img class="logo" src="{{ url ('../images/' . $s->header_logo) }}">
                    @endforeach
               </div>

               <div class="admin">
                    <h3 class="m-0">Administrator</h3>
               </div>
          </div>

          <div class="d-flex align-item-center">
               <div class="d-flex">
                    <a href="{{url ('/admin/login')}}" class="logout"><span class="material-symbols-rounded">logout</span> Logout</a>
               </div>
          </div>
     </div>

     <div class="section">
          <a class="linkword" href="{{url ('/admin/home')}}">
               <div class="link">
                    <span class="material-symbols-outlined">home</span>
                    Home
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/new')}}">
               <div class="link">
                    <span class="material-symbols-outlined">diamond</span>
                    New
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/about')}}">
               <div class="link active">
                    <span class="material-symbols-outlined">info</span>
                    About
               </div>
          </a>

          <a class="linkword" href="{{url ('/admin/faqs')}}">
               <div class="link">
                    <span class="material-symbols-outlined">help</span>
                    FAQs
               </div> 
          </a>

          <a class="linkword" href="{{url ('/admin/settings')}}">
               <div class="link">
                    <span class="material-symbols-outlined">settings</span>
                    Settings
               </div> 
          </a>
     </div>

     <div class="content">
          <div class="col-sm-12">  
               <script>
                    @if(Session::has('success'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false,
                              
                         }
  		               toastr.success("{{ session('success') }}");
                    @endif

                    @if(Session::has('deleted'))
                         toastr.options =
                         {
  	                         "closeButton" : false,
  	                         "progressBar" : false
                         }
  		               toastr.error("{{ session('deleted') }}");
                    @endif
               </script>

               <div class="d-flex justify-content-between align-items-center">
                    <div>
                         <h2>Mastermind of the brand</h2>
                    </div>

                    <div>
                         <button class="btn btn-submit shadow" data-bs-toggle="modal" data-bs-target="#addmasterModal">
                              <div class="d-flex align-items-center justify-content-center mt-1">
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">add_circle</span>
                                        </div>

                                        <div>
                                             Add New
                                        </div>
                                   </div>
                              </div>
                         </button>
                    </div>
               </div>

               <div class="mt-5">
                    <div class="d-flex align-items-start flex-wrap">
                         @foreach($mastermind as $m)
                              <div class="card m shadow" style="width: 18rem;">
                                   <div class="d-flex justify-content-end p-3">
                                        <div>
                                             <form action="/delete-master" method="POST">
                                                  @csrf

                                                  <input type="hidden" value="{{ $m->m_id }}" name="m_id" required>

                                                  <button type="submit" class="edit-short" onclick="return confirm('Are you sure you want to delete this entry?');"><span class="material-symbols-outlined">delete</span></button>
                                             </form>
                                        </div>
                                   </div>

                                   <center>
                                        <img class="card-img-top m-image shadow" src="{{ url('/images/' . $m->m_image) }}" alt="Card image cap">

                                        <div class="card-body">
                                             <h5>{{ $m->m_fname }} {{ $m->m_lname }}</h5>
                                             <p class="card-text">{{ $m->m_position }}</p>
                                             <a href="{{ $m->m_profile }}" target="_blank" class="btn btn-fb mt-3"><span class="material-symbols-outlined">account_circle</span>View Profile</a>
                                        </div>
                                   </center>
                              </div>
                         @endforeach
                    </div>
               </div>

               @if ($mastermind->isEmpty())
                         <div class="d-flex flex-column align-items-center">
                              <div>
                                   <img src="{{ url('../images/master.png')}}" class="empty">
                              </div>

                              <div>
                                   <p class="empty_note">Time to add some stuff in here!</p>
                              </div>
                         </div>
               @endif

               <div class="modal fade" id="addmasterModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                    <div class="modal-dialog">
                         <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Add Owners and Co-Owners</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                         </div>

                         <div class="modal-body">
                              <form action="/add-master" method="post" enctype="multipart/form-data">
                                   @csrf
                                   
                                        <div class="form-group pt-2">
                                             <div class="row">
                                                  <div class="col-sm-6">
                                                       <label class="form-label">First name</label>
                                                       <input type="text" class="form-control" name="m_fname" required>
                                                  </div>

                                                  <div class="col-sm-6">
                                                       <label class="form-label">Last name</label>
                                                       <input type="text" class="form-control" name="m_lname" required>
                                                  </div>
                                             </div>

                                             <div class="row">
                                                  <div class="col-sm-6">
                                                       <label class="form-label">Position</label>
                                                       <select class="form-select" name="m_position">
                                                            <option value="Owner">Owner</option>
                                                            <option value="Co-Owner">Co-Owner</option>
                                                       </select>
                                                  </div>

                                                  <div class="col-sm-6">
                                                       <label class="form-label">Image</label>
                                                       <input type="file" class="form-control" accept="images/*" name="m_image" required>
                                                  </div>
                                             </div>
                                             
                                             <div class="row">
                                                  <div class="col-sm-12">
                                                       <label class="form-label">Facebook profile</label>
                                                       <input type="text" class="form-control" name="m_profile" required>
                                                  </div>
                                             </div>
                                        </div>

                                        <div class="form-group pt-2 mt-3">
                                             <div class="d-flex justify-content-end">
                                                  <div>
                                                       <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                            <div class="d-flex align-items-center justify-content-center">
                                                                 <div class="d-flex align-items-start">
                                                                      <div>
                                                                      <span class="material-symbols-outlined">cancel</span>
                                                                      </div>

                                                                      <div>
                                                                           Cancel
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </button>
                                                  </div>

                                                  <div>
                                                       <button class="btn btn-add" type="submit">
                                                            <div class="d-flex align-items-center justify-content-center">
                                                                 <div class="d-flex align-items-start">
                                                                      <div>
                                                                      <span class="material-symbols-outlined">add_circle</span>
                                                                      </div>

                                                                      <div>
                                                                           Add
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </button>
                                                  </div>
                                             </div>
                                        </div>
                              </form>
                         </div>
                         </div>
                    </div>
               </div>

               <div class="d-flex justify-content-between align-items-center mt-5 pt-5">
                    <div>
                         <h2>Workers</h2>
                    </div>

                    <div>
                         <button class="btn btn-submit shadow" data-bs-toggle="modal" data-bs-target="#addworkerModal">
                              <div class="d-flex align-items-center justify-content-center mt-1">
                                   <div class="d-flex align-items-start">
                                        <div>
                                             <span class="material-symbols-outlined">add_circle</span>
                                        </div>

                                        <div>
                                             Add New
                                        </div>
                                   </div>
                              </div>
                         </button>
                    </div>
               </div>


               <div class="mt-5 mb-5">
                    <div class="d-flex align-items-start flex-wrap">
                         @foreach($worker as $w)
                              <div class="card m shadow" style="width: 18rem;">
                                   <div class="d-flex justify-content-end p-3">
                                        <div>
                                             <form action="/delete-worker" method="POST">
                                                  @csrf

                                                  <input type="hidden" value="{{ $w->w_id }}" name="w_id">

                                                  <button type="submit" class="edit-short" onclick="return confirm('Are you sure you want to delete this entry?');"><span class="material-symbols-outlined">delete</span></button>
                                             </form>
                                        </div>
                                   </div>

                                   <center>
                                        <img class="card-img-top m-image shadow" src="{{ url('/images/' . $w->w_image) }}" alt="Card image cap">

                                        <div class="card-body">
                                             <h5>{{ $w->w_fname }} {{ $w->w_lname }}</h5>
                                             <p class="card-text">{{ $w->w_position }}</p>
                                             <a href="{{ $w->w_profile }}" target="_blank" class="btn btn-fb mt-3"><span class="material-symbols-outlined">account_circle</span>View Profile</a>
                                        </div>
                                   </center>
                              </div>
                         @endforeach
                    </div>
               </div>

               @if ($worker->isEmpty())
                         <div class="d-flex flex-column align-items-center mb-5">
                              <div>
                                   <img src="{{ url('../images/worker.png')}}" class="empty">
                              </div>

                              <div>
                                   <p class="empty_note">Time to add some stuff in here!</p>
                              </div>
                         </div>
               @endif

               <div class="modal fade" id="addworkerModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                    <div class="modal-dialog">
                         <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Add Worker</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                         </div>

                         <div class="modal-body">
                              <form action="/add-worker" method="post" enctype="multipart/form-data">
                                   @csrf
                                   
                                        <div class="form-group pt-2">
                                             <div class="row">
                                                  <div class="col-sm-6">
                                                       <label class="form-label">First name</label>
                                                       <input type="text" class="form-control" name="w_fname" required>
                                                  </div>

                                                  <div class="col-sm-6">
                                                       <label class="form-label">Last name</label>
                                                       <input type="text" class="form-control" name="w_lname" required>
                                                  </div>
                                             </div>

                                             <div class="row">
                                                  <div class="col-sm-6">
                                                       <label class="form-label">Position</label>
                                                       <select class="form-select" name="w_position">
                                                            <option value="Tailor">Tailor</option>
                                                            <option value="Master Cutter">Master Cutter</option>
                                                            <option value="Laundress">Laundress</option>
                                                            <option value="Clothes Ironer">Clothes Ironer</option>
                                                       </select>
                                                  </div>

                                                  <div class="col-sm-6">
                                                       <label class="form-label">Image</label>
                                                       <input type="file" class="form-control" accept="images/*" name="w_image" required>
                                                  </div>
                                             </div>
                                             
                                             <div class="row">
                                                  <div class="col-sm-12">
                                                       <label class="form-label">Facebook profile</label>
                                                       <input type="text" class="form-control" name="w_profile" required>
                                                  </div>
                                             </div>
                                        </div>

                                        <div class="form-group pt-2 mt-3">
                                             <div class="d-flex justify-content-end">
                                                  <div>
                                                       <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                            <div class="d-flex align-items-center justify-content-center">
                                                                 <div class="d-flex align-items-start">
                                                                      <div>
                                                                      <span class="material-symbols-outlined">cancel</span>
                                                                      </div>

                                                                      <div>
                                                                           Cancel
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </button>
                                                  </div>

                                                  <div>
                                                       <button class="btn btn-add" type="submit">
                                                            <div class="d-flex align-items-center justify-content-center">
                                                                 <div class="d-flex align-items-start">
                                                                      <div>
                                                                      <span class="material-symbols-outlined">add_circle</span>
                                                                      </div>

                                                                      <div>
                                                                           Add
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </button>
                                                  </div>
                                             </div>
                                        </div>
                              </form>
                         </div>
                         </div>
                    </div>
               </div>
          </div>

          <div class="card w-75 shadow mt-5 mb-5">
               <div class="card-header">
                    <h5 class="card-title">About</h5>
               </div>
                         
               <div class="card-body">
                    @foreach($about as $a)
                         <div class="d-flex align-items-start">
                              <div>
                                   <p class="card-text">{{ $a->about }}</p>
                              </div>
                         </div>

                         <div class="d-flex justify-content-end mt-3">
                              <div>
                                   <button class="btn btn-short shadow mt-3" data-bs-toggle="modal" data-bs-target="#aboutModal">
                                        <div class="d-flex align-items-center justify-content-center mt-1">
                                             <div class="d-flex align-items-start">
                                                  <div>
                                                       <span class="material-symbols-outlined">edit_note</span>
                                                  </div>

                                                  <div>
                                                       Update
                                                  </div>
                                             </div>
                                        </div>
                                   </button>
                              </div>
                         </div>
                    @endforeach
               </div>
          </div>

          <div class="modal fade" id="aboutModal" tabindex="-1" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
               <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Update About</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                         <form action="/update-about" method="post" enctype="multipart/form-data">
                              @csrf
                              
                                   <div class="form-group pt-2">
                                   <label for="new_name" class="form-label">Description</label>
                                   <textarea class="form-control about-text" name="about" required></textarea>
                                   </div>

                                   <div class="form-group pt-2 mt-3">
                                        <div class="d-flex justify-content-end">
                                             <div>
                                                  <button type="button" class="btn btn-delete" data-bs-dismiss="modal" id="cancel">
                                                       <div class="d-flex align-items-center justify-content-center">
                                                            <div class="d-flex align-items-start">
                                                                 <div>
                                                                 <span class="material-symbols-outlined">cancel</span>
                                                                 </div>

                                                                 <div>
                                                                      Cancel
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </button>
                                             </div>

                                             <div>
                                                  <button class="btn btn-add" type="submit">
                                                       <div class="d-flex align-items-center justify-content-center">
                                                            <div class="d-flex align-items-start">
                                                                 <div>
                                                                 <span class="material-symbols-outlined">edit_note</span>
                                                                 </div>

                                                                 <div>
                                                                      Update
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </button>
                                             </div>
                                        </div>
                                   </div>
                         </form>
                    </div>
                    </div>
               </div>
          </div>
     </div>
</body>
</html>