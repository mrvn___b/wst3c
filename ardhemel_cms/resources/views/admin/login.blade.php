<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>ADMIN LOGIN | ADMIN</title>
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          body {
               background-image: url('../images/login_bg.jpg');
               background-size: cover;
          }

          .login {
               margin-top: 130px;
               background-color: #ffffff;
               width: 400px;
          }

          .form-control {
               width: 100%;
          }

          .form-control:focus {
                box-shadow: 0 0 0 0.2rem rgba(111, 135, 986, 0.25);
            }

          .login > div {
               width: 100%;
          }

          .btn-login {
               background-color: #6F8762;
               color: #ffffff;
               width: 100%;
               margin-top: 10px;
          }

          .btn-login:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          .btn-login:focus {
               box-shadow: 0 0 0 0.2rem rgba(111, 135, 986, 0.25);
          }

          .error {
               color: red;
               font-size: 12px;
          }
     </style>
</head>
<body>
<div class="d-flex justify-content-center">
            <div class="login d-inline-flex p-5 shadow">
                <div>
                    <div class="mb-3">
                        <h3><b>Login</b></h3>
                    </div>

                    @if(Session::get('invalid'))
                            <div class = "alert alert-danger p-2">
                                <p>{{Session::get('invalid')}}</p>
                            </div>
                    @endif

                    <form action="/admin/home" method="POST">
                        @csrf
                        
                        <label for="username" class="form-label mt-3">Username</label>
                        <input type="text" class="form-control" name="username" value="{{old ('username')}}">
                        <p class="error">@error ('username'){{$message}} @enderror</p>

                        <label for="password" class="mt-1 form-label">Password</label>
                        <input type="password" class="form-control" name="password" value="{{old ('password')}}">
                        <p class="error">@error ('password'){{$message}} @enderror</p>

                        <input type="submit" class="btn btn-login" name="submit" value="LOGIN">
                    </form>
                </div>
            </div>
        </div>
</body>
</html>