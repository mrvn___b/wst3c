<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
     <title>ABOUT | ARDHEMEL</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               background-color: #363636;
               position: fixed;
               width: 100%;
               z-index: 1;
          }

          .nav > div {
               padding: 15px;
               padding-left: 80px;
               padding-right: 80px;
          }

          .navigator {
               font-weight: 400;
               text-decoration: none;
               color: white;
               margin-left: 50px;
               padding-bottom: 5px;
          }

          .footer {
               background-color: #363636;
               width: 100%;
          }

          .footer > div {
               padding: 15px;
               padding-left: 80px;
               padding-right: 80px;
          }

          .footer-text {
               color: white;
               margin: 0;
          }

          .footer-icon {
               color: white;
               font-size: 40px;
          }

          .active {
               font-weight: 400;
               color: white;
               border-bottom: 3px solid white;
          }

          .navigator:hover {
               font-weight: 400;
               color: white;
               border-bottom: 3px solid white;
          }

          .logo {
               height: 30px;
          }

          .name {
               color: #ffffff;
               margin-left: 20px;
               border-left: 1px solid #ffffff;
               padding-left: 20px;
          }

          .section {
               padding: 80px 120px;
          }

          .heading-text {
               font-size: 40px;
               font-weight: bold;
               padding-bottom: 5px;
               border-bottom: 10px solid #6F8762;
               border-radius: 5px;
               margin: 0;
               
          }

          .holder {
               text-align: center;
          }

          .m-image {
               width: 150px;
               height: 150px;
               border-radius: 75px;
               object-fit: cover;
          }

          .btn-fb {
               width: 100%;
               background-color: #6F8762;
               border-radius: 25px;
               color: #ffffff;
          }

          .btn-fb:hover {
               background-color: #495C40;
               color: #ffffff;
          }

          span {
               vertical-align: middle;
               margin-right: 10px;
          }

          .master {
               margin-bottom: 100px;
          }

          .worker {
               margin-bottom: 100px;
          }

          span {
               vertical-align: middle;
               margin-right: 10px;
          }

          .contact {
               color: white;
               font-size: 20px;
               vertical-align: middle;
          }

          .contact-text {
               color: white;
               margin: 0;
          }

          .about-p {
               margin: 35px 200px 0 200px;
               text-align: justify;
               text-indent: 50px;
          }

          
          .back {
               padding: 1rem;
               background-color: #6F8762;
               width: 3rem;
               height: 3rem;
               border-radius: 50%;
               position: fixed;
               z-index: 3;
               margin: 1.5rem;
               right: 0;
               bottom: 0;
          }

          .back:hover {
               background-color: #495C40;
          }

          .bx-up-arrow-alt {
               color: #FFFFFF;
               vertical-align:text-top;
               font-size: 1rem;
          }
     </style>
</head>
<body>
     <a href="#nav">
          <div class="back shadow">
               <i class='bx bx-up-arrow-alt'></i>
          </div>
     </a>

     <div class="d-flex justify-content-between align-items-center nav">
          <div class="d-flex align-items-center">
               <div>
                    @foreach($settings as $s)
                    <img class="logo" src="{{ url ('../images/' . $s->header_logo) }}">
                    @endforeach
               </div>

               <div class="name">
                    <h3 class="m-0">Ardhemel</h3>
               </div>
          </div>

          <div class="d-flex">
               <div>
                    <a class="navigator" href="{{url ('/home')}}">Home</a>
               </div>

               <div>
                    <a class="navigator active" href="{{url ('/about')}}">About</a>
               </div>

               <div>
                    <a class="navigator" href="{{url ('/faqs')}}">FAQs</a>
               </div>
          </div>
     </div>

     <div class="section">
          <h1 class="holder" id="nav"><span class="heading-text">Mastermind of the Brand</span></h1>

          <div class="d-flex align-items-start flex-wrap mt-5 justify-content-center master">
               @foreach($mastermind as $m)
                    <div class="card m shadow" style="width: 18rem; margin: 20px">
                         <div class="p-3">
                              <center>
                                   <img class="card-img-top m-image shadow" src="{{ url('/images/' . $m->m_image) }}">

                                   <div class="card-body">
                                        <h5>{{ $m->m_fname }} {{ $m->m_lname }}</h5>
                                        <p class="card-text">{{ $m->m_position }}</p>
                                        <a href="{{ $m->m_profile }}" target="_blank" class="btn btn-fb mt-3"><span class="material-symbols-outlined">account_circle</span>View Profile</a>
                                   </div>
                              </center>
                         </div>
                    </div>
               @endforeach
          </div>

          <h1 class="holder"><span class="heading-text">The Employees</span></h1>

          <div class="d-flex align-items-start flex-wrap mt-5 justify-content-center flex-wrap worker">
               @foreach($worker as $w)
                    <div class="card m shadow" style="width: 18rem; margin: 20px">
                         <div class="p-3">
                              <center>
                                   <img class="card-img-top m-image shadow" src="{{ url('/images/' . $w->w_image) }}">

                                   <div class="card-body">
                                        <h5>{{ $w->w_fname }} {{ $w->w_lname }}</h5>
                                        <p class="card-text">{{ $w->w_position }}</p>
                                        <a href="{{ $w->w_profile }}" target="_blank" class="btn btn-fb mt-3"><span class="material-symbols-outlined">account_circle</span>View Profile</a>
                                   </div>
                              </center>
                         </div>
                    </div>
               @endforeach
          </div>

          <h1 class="holder"><span class="heading-text">A Glimpse About Us</span></h1>

          <div class="d-flex align-items-start flex-wrap mt-5 justify-content-center worker">
               @foreach($about as $a)
                    <p class="about-p">{{ $a->about }}</p>
               @endforeach
          </div>
     </div>

     <div class="d-flex justify-content-between align-items-center footer">
          <div class="d-flex align-items-center flex-column">
               <div ><p class="footer-text">Find us on:</p></div>

               <div class="d-flex align-items-center mt-2">
                    <div style="margin-right: 10px">
                         @foreach($settings as $s)
                              <a href="{{ $s->fb_link }}" target="_blank"><i class='bx bxl-facebook-circle footer-icon'></i></a>
                         @endforeach
                    </div>

                    <div>
                         @foreach($settings as $s)
                              <a href="{{ $s->shopee_link }}" target="_blank"><i class='bx bxs-shopping-bag footer-icon'></i></a>
                         @endforeach
                    </div>
               </div>
          </div>

          <div class="d-flex align-items-center flex-column">
               <p class="footer-text">All right reserved | Copyright <?php echo $dateYear = date('Y'); ?></p>
               <p class="footer-text">Created by: Marvin Bautista | BSIT-3C</p>
          </div>

          <div class="d-flex flex-column align-items-end">
               <div>
                    @foreach($settings as $s)
                         <img class="logo" src="{{ url ('../images/' . $s->footer_logo) }}">
                    @endforeach
               </div>

               <div class="mt-3">
                    <i class='bx bxl-gmail contact' ></i>
                    @foreach($settings as $s)
                         <span class="contact-text">{{ $s->email }}</span>
                    @endforeach
               </div>

               <div>
                    <i class='bx bxs-phone contact' ></i>
                    @foreach($settings as $s)
                         <span class="contact-text">{{ $s->phone_number }}</span>
                    @endforeach
               </div>
          </div>
     </div>
</body>
</html>