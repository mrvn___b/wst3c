<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function viewLogin() {
        return view('admin.login');
    }

    public function loggedIn(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = DB::table('admin')->where('username', $username)->where('password', $password);

        if($user->count() == 1) {
            return redirect('/admin/home');
        }
        
        return back()->with('invalid', 'Incorect username or password.');
    }
}
