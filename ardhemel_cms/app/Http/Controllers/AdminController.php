<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function AdminHome() {
        $data = array(
            'banner'=>DB::table('banner')->get(),
            'settings'=>DB::table('settings')->get(),
        );

        return view('admin.home', $data);
    }

    public function AdminNew() {
        $data = array(
            'new'=>DB::table('new_item')->orderBy('new_id', 'desc')->get(),
            'settings'=>DB::table('settings')->get(),
        );

        return view('admin.new', $data);
    }

    public function AdminAbout() {
        $data = array(
            'mastermind'=>DB::table('mastermind')->get(),
            'worker'=>DB::table('worker')->get(),
            'settings'=>DB::table('settings')->get(),
            'about'=>DB::table('about')->get(),
        );

        return view('admin.about', $data);
    }

    public function AdminFaqs() {
        $data = array(
            'faq'=>DB::table('faqs')->get(),
            'settings'=>DB::table('settings')->get(),
        );

        return view('admin.faqs', $data);
    }

    public function AdminSettings() {
        $data = array(
            'settings'=>DB::table('settings')->get(),
            'admin'=>DB::table('admin')->get(),
        );

        return view('admin.settings', $data);
    }
}
