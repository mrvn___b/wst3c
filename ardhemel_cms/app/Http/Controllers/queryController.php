<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class queryController extends Controller
{
    public function addFaqs(Request $request) {
        $request->validate([
            'question' => 'required',
            'answer' => 'required',
        ]);

        $question = $request->input('question');
        $answer = $request->input('answer');

        DB::table('faqs')->insert([
            'question'=>$question,
            'answer'=>$answer,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function deleteFaqs(Request $request) {
        $faq_id = $request->input('faq_id');

        $delete = DB::table('faqs')->where('faq_id', $faq_id)->delete();

        if($delete) {
            return back()->with('deleted', 'Entry deleted.');
        }
    }

    public function addNew(Request $request) {
        $new_name = $request->input('new_name');
        $new_desc = $request->input('new_desc');
        $new_link = $request->input('new_link');
        $new_price = $request->input('new_price');
        $newFile = $request->file('new_image');

        $imageName = time().'.'.$request->new_image->extension();
        $path = $request->new_image->move(public_path('images'), $imageName);

        DB::table('new_item')->insert([
            'new_item'=>$new_name,
            'new_desc'=>$new_desc,
            'new_image'=>$imageName,
            'new_link'=>$new_link,
            'new_price'=>$new_price,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function hideNew(Request $request) {
        $new_id = $request->input('new_id');

        DB::table('new_item')->where('new_id', $new_id)->update([
            'new_status'=>1,
        ]);

        return back()->with('deleted', 'Entry hidden');
    }

    public function showNew(Request $request) {
        $new_id = $request->input('new_id');

        DB::table('new_item')->where('new_id', $new_id)->update([
            'new_status'=>0,
        ]);

        return back()->with('success', 'Entry shown');
    }

    public function addMaster(Request $request) {
        $m_fname = $request->input('m_fname');
        $m_lname = $request->input('m_lname');
        $m_position = $request->input('m_position');
        $m_profile = $request->input('m_profile');
        $m_image = $request->file('image');

        $imageName = time().'.'.$request->m_image->extension();
        $path = $request->m_image->move(public_path('images'), $imageName);

        DB::table('mastermind')->insert([
            'm_fname'=>$m_fname,
            'm_lname'=>$m_lname,
            'm_position'=>$m_position,
            'm_profile'=>$m_profile,
            'm_image'=>$imageName,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function deleteMaster(Request $request) {
        $m_id = $request->input('m_id');

        $delete = DB::table('mastermind')->where('m_id', $m_id)->delete();

        if($delete) {
            return back()->with('deleted', 'Entry deleted.');
        }
    }

    public function addWorker(Request $request) {
        $w_fname = $request->input('w_fname');
        $w_lname = $request->input('w_lname');
        $w_position = $request->input('w_position');
        $w_profile = $request->input('w_profile');
        $w_image = $request->file('w_image');

        $imageName = time().'.'.$request->w_image->extension();
        $path = $request->w_image->move(public_path('images'), $imageName);

        DB::table('worker')->insert([
            'w_fname'=>$w_fname,
            'w_lname'=>$w_lname,
            'w_position'=>$w_position,
            'w_profile'=>$w_profile,
            'w_image'=>$imageName,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function updateAbout(Request $request) {
        $about = $request->input('about');

        DB::table('about')->update([
            'about'=>$about,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function deleteWorker(Request $request) {
        $w_id = $request->input('w_id');

        $delete = DB::table('worker')->where('w_id', $w_id)->delete();

        if($delete) {
            return back()->with('deleted', 'Entry deleted.');
        }
    }

    public function bannerUpload(Request $request) {
        $banner_title = $request->input('banner_title');
        $banner_desc = $request->input('banner_desc');
        $banner_color = $request->input('banner_color');
        $banner_image = $request->file('banner_image');

        $imageName = time().'.'.$request->banner_image->extension();
        $path = $request->banner_image->move(public_path('images'), $imageName);

        DB::table('banner')->where('banner_id', 1)->update([
            'banner_pic' => $imageName,
            'banner_title' => $banner_title,
            'banner_color' => $banner_color,
            'banner_desc' => $banner_desc,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateFb(Request $request) {
        $fb_link = $request->input('fb_link');

        DB::table('settings')->update([
            'fb_link' => $fb_link,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateShopee(Request $request) {
        $shopee_link = $request->input('shopee_link');

        DB::table('settings')->update([
            'shopee_link' => $shopee_link,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateEmail(Request $request) {
        $email = $request->input('email');

        DB::table('settings')->update([
            'email' => $email,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updatePhone(Request $request) {
        $phone = $request->input('phone');

        DB::table('settings')->update([
            'phone_number' => $phone,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateHeader(Request $request) {
        $header = $request->file('header');

        $imageName = time().'.'.$request->header->extension();
        $path = $request->header->move(public_path('images'), $imageName);

        DB::table('settings')->update([
            'header_logo' => $imageName,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateFooter(Request $request) {
        $footer = $request->file('footer');

        $imageName = time().'.'.$request->footer->extension();
        $path = $request->footer->move(public_path('images'), $imageName);

        DB::table('settings')->update([
            'footer_logo' => $imageName,
        ]);

        return back()->with('success', 'Successfully updated!');
    }

    public function updateAdmin(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        DB::table('admin')->update([
            'username' => $username,
            'password' => $password
        ]);

        return back()->with('success', 'Successfully updated!');
    }
}
