<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function userHome() {
        $data = array(
            'settings'=>DB::table('settings')->get(),
            'banner'=>DB::table('banner')->get(),
            'new'=>DB::table('new_item')->where('new_status', 0)->orderBy('new_id', 'desc')->get(),
        );

        return view('home', $data);
    }

    public function userAbout() {
        $data = array(
            'settings'=>DB::table('settings')->get(),
            'mastermind'=>DB::table('mastermind')->get(),
            'worker'=>DB::table('worker')->get(),
            'about'=>DB::table('about')->get(),
        );

        return view('about', $data);
    }

    public function userGallery() {

        return view('gallery');
    }

    public function userFaqs() {
        $data = array(
            'settings'=>DB::table('settings')->get(),
            'faqs'=>DB::table('faqs')->get(),
        );

        return view('faqs', $data);
    }

    public function userContact() {
        $data = array(
            'settings'=>DB::table('settings')->get(),
        );

        return view('contact', $data);
    }


}
