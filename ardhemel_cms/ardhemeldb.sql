-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2022 at 04:18 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ardhemeldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `a_id` int(11) NOT NULL,
  `about` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`a_id`, `about`) VALUES
(1, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae fuga necessitatibus accusamus, tempora voluptatum placeat qui iusto fugiat facilis ea nulla eveniet recusandae nam aliquam nobis sequi, quidem nisi commodi! Pariatur rerum quis magnam laudantium corrupti cum dolor maiores deserunt accusamus, ipsam consectetur vero neque id non impedit odit porro quisquam beatae, asperiores suscipit inventore minima error quia sunt! Rerum voluptate eveniet cumque consequuntur deserunt fugiat ut id? Modi magnam suscipit obcaecati voluptatem assumenda quasi recusandae optio cumque dolorem sint dolor, quidem perferendis illo expedita earum. Laborum saepe facere ipsum minima labore explicabo dolorem velit architecto, est eaque voluptate perspiciatis? At dolorem deserunt sapiente est blanditiis. In est dicta eius, fugit consectetur numquam, quidem doloremque maxime quaerat deserunt corporis enim, rem ea accusantium harum minima quisquam? Aspernatur laboriosam quae natus ad saepe dolorem, sunt cupiditate recusandae asperiores odio minima expedita accusamus, quasi suscipit incidunt veritatis a, ipsum qui temporibus illum.');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL,
  `username` varchar(8) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_title` text NOT NULL,
  `banner_desc` text NOT NULL,
  `banner_color` varchar(50) NOT NULL,
  `banner_pic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_title`, `banner_desc`, `banner_color`, `banner_pic`) VALUES
(1, 'Summer is Up!', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', '#292929', '1655461164.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `faq_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`faq_id`, `question`, `answer`) VALUES
(31, 'Where is your store located?', 'We are located at Purok 1, San Vicente, San Manuel, Tarlac. We also have our online store in Shopee and Facebook. We are glad to have you here at our physical store as well as in virtual!'),
(32, 'What type of products or services you have?', 'We offer sublimation printing, bulk order of apparels, custom print design, and selling of trendy clothes and shorts.'),
(33, 'When is your store established?', 'Ardhemel Apparel was established since 2003.'),
(34, 'What is the meaning of your brand?', 'The brand is derived from the members of the family: ARnulfo, DHerick, and iMELda.'),
(35, 'Can I be a reseller?', 'Yes, you can be a reseller, just contact as through our Facebook page, email, contact number, or visit our physical shop for inquiries.');

-- --------------------------------------------------------

--
-- Table structure for table `mastermind`
--

CREATE TABLE `mastermind` (
  `m_id` int(11) NOT NULL,
  `m_fname` varchar(50) NOT NULL,
  `m_lname` varchar(50) NOT NULL,
  `m_position` varchar(100) NOT NULL,
  `m_profile` text NOT NULL,
  `m_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mastermind`
--

INSERT INTO `mastermind` (`m_id`, `m_fname`, `m_lname`, `m_position`, `m_profile`, `m_image`) VALUES
(10, 'Marvin', 'Bautista', 'Owner', 'https://web.facebook.com/mrvn.bae/', '1655259862.jpg'),
(14, 'Christelle', 'Gabuyo', 'Co-Owner', 'https://web.facebook.com/telledianne.gabuyo', '1655430987.jpg'),
(15, 'Jan Patrick', 'Urbano', 'Co-Owner', 'https://web.facebook.com/Accezen', '1655431073.jpg'),
(16, 'Aisle Lush', 'Valdez', 'Co-Owner', 'https://web.facebook.com/Aylvee', '1655431152.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `new_item`
--

CREATE TABLE `new_item` (
  `new_id` int(11) NOT NULL,
  `new_item` text NOT NULL,
  `new_desc` text NOT NULL,
  `new_price` float NOT NULL,
  `new_image` text NOT NULL,
  `new_link` text NOT NULL,
  `new_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `new_item`
--

INSERT INTO `new_item` (`new_id`, `new_item`, `new_desc`, `new_price`, `new_image`, `new_link`, `new_status`) VALUES
(40, 'Red Velvet Korean Dress', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', 1000, '1655430532.jpg', 'http://shopee.ph', 0),
(41, 'Floral Dress', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', 259, '1655430568.jpg', 'http://shopee.ph', 0),
(42, 'Naruto Printed Shirt', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', 150, '1655430606.jpg', 'http://shopee.ph', 1),
(43, 'Anime Shirt', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', 189, '1655430633.jpg', 'http://shopee.ph', 1),
(44, 'White Dress', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, sapiente illo atque commodi cum sequi! Alias totam modi, doloribus vero quisquam error laudantium reiciendis asperiores pariatur excepturi assumenda enim. Illum rerum, repellat obcaecati quas delectus pariatur! Alias, obcaecati asperiores id rem quod assumenda minima possimus quas explicabo necessitatibus sint soluta?', 150, '1655430681.png', 'http://shopee.ph', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `fb_link` text NOT NULL,
  `shopee_link` text NOT NULL,
  `phone_number` text NOT NULL,
  `email` text NOT NULL,
  `header_logo` text NOT NULL,
  `footer_logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `fb_link`, `shopee_link`, `phone_number`, `email`, `header_logo`, `footer_logo`) VALUES
(1, 'https://web.facebook.com/Ardhemel-Apparel-106190238128137', 'https://shopee.ph/ardhemelapparel?categoryId=100011&itemId=7693579907', '(+63) 9166646306', 'ardhemel@gmail.com', '1655444001.png', '1655443979.png');

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE `worker` (
  `w_id` int(11) NOT NULL,
  `w_fname` varchar(50) NOT NULL,
  `w_lname` varchar(50) NOT NULL,
  `w_position` varchar(100) NOT NULL,
  `w_profile` text NOT NULL,
  `w_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `worker`
--

INSERT INTO `worker` (`w_id`, `w_fname`, `w_lname`, `w_position`, `w_profile`, `w_image`) VALUES
(3, 'Christelle', 'Gabuyo', 'Tailor', 'https://web.facebook.com/Accezen', '1655300375.png'),
(4, 'Julie Ann', 'Dela Cruz', 'Master Cutter', 'https://web.facebook.com/Accezen', '1655300404.jpg'),
(5, 'Anna', 'Cruz', 'Laundress', 'https://web.facebook.com/Accezen', '1655346148.png'),
(8, 'Julie Ann', 'Dela Cruz', 'Clothes Ironer', 'https://web.facebook.com/Accezen', '1655431027.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `mastermind`
--
ALTER TABLE `mastermind`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `new_item`
--
ALTER TABLE `new_item`
  ADD PRIMARY KEY (`new_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`w_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `mastermind`
--
ALTER TABLE `mastermind`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `new_item`
--
ALTER TABLE `new_item`
  MODIFY `new_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `worker`
--
ALTER TABLE `worker`
  MODIFY `w_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
