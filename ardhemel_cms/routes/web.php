<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\queryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/login', [LoginController::class, 'viewLogin']);

Route::post('/admin/home', [LoginController::class, 'loggedIn']);

Route::get('/admin/home', [AdminController::class, 'AdminHome']);

Route::get('/admin/new', [AdminController::class, 'AdminNew']);

Route::get('/admin/about', [AdminController::class, 'AdminAbout']);

Route::get('/admin/faqs', [AdminController::class, 'AdminFaqs']);

Route::get('/admin/settings', [AdminController::class, 'AdminSettings']);

Route::post('/add-faqs', [queryController::class, 'addFaqs']);

Route::post('/delete-faqs', [queryController::class, "deleteFaqs"]);

Route::post('/add-new', [queryController::class, 'addNew']);

Route::post('/hide-new', [queryController::class, "hideNew"]);

Route::post('/show-new', [queryController::class, "showNew"]);

Route::post('/add-master', [queryController::class, "addMaster"]);

Route::post('/delete-master', [queryController::class, "deleteMaster"]);

Route::post('/update-about', [queryController::class, "updateAbout"]);

Route::post('/add-worker', [queryController::class, "addWorker"]);

Route::post('/delete-worker', [queryController::class, "deleteWorker"]);

Route::post('/banner-upload', [queryController::class, "bannerUpload"]);

Route::post('/update-fb', [queryController::class, "updateFb"]);

Route::post('/update-shopee', [queryController::class, "updateShopee"]);

Route::post('/update-email', [queryController::class, "updateEmail"]);

Route::post('/update-phone', [queryController::class, "updatePhone"]);

Route::post('/update-header', [queryController::class, "updateHeader"]);

Route::post('/update-footer', [queryController::class, "updateFooter"]);

Route::post('/update-admin', [queryController::class, "updateAdmin"]);

Route::get('/home', [UserController::class, "userHome"]);

Route::get('/gallery', [UserController::class, "userGallery"]);

Route::get('/about', [UserController::class, "userAbout"]);

Route::get('/faqs', [UserController::class, "userFaqs"]);

Route::get('/contact', [UserController::class, "userContact"]);