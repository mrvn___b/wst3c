<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function customer($custID, $custName, $custAddress){
        return view('customer',
            ['custID'=>$custID, 
            'custName'=>$custName, 
            'custAddress'=>$custAddress]);
    }

    public function item($itemNo, $itemName, $itemPrice){
        return view('item', 
            ['itemNo'=>$itemNo, 
            'itemName'=>$itemName, 
            'itemPrice'=>$itemPrice]);
    }

    public function order($custID, $custName, $orderNo, $orderDate){
        return view('order', 
            ['custID'=>$custID, 
            'custName'=>$custName, 
            'orderNo'=>$orderNo, 
            'orderDate'=>$orderDate]);
    }

    public function orderDetails($transNo, $orderNo, $itemID, $itemName, $itemPrice, $itemQty){
        return view('order-details', 
            ['transNo'=>$transNo, 
            'orderNo'=>$orderNo, 
            'itemID'=>$itemID, 
            'itemName'=>$itemName, 
            'itemPrice'=>$itemPrice, 
            'itemQty'=>$itemQty]);
    }
}
