<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=\, initial-scale=1.0">
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
          <title>Order Details</title>
     </head>

     <body class="m-5">
          <div class="d-flex flex-row">
               <div class="p-2"><a href="{{ url ('/customer/120/Marvin/San Juan')}}">Customer</a></div>
               <div class="p-2"><a href="{{ url ('/item/10/Milk/300')}}">Item</a></div>
               <div class="p-2"><a href="{{ url ('/order/120/Marvin/12/02-22-2022')}}">Order</a></div>
               <div class="p-2"><a href="{{ url ('/order-details/10/121/201/Milk/300/2')}}">Order Details</a></div>
          </div>

          <h1 class="mb-5">Order Details</h1>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Transaction No</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value=<?php echo $transNo;?> readonly>
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Order Number</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value=<?php echo $orderNo;?> readonly>
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Item ID</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value='<?php echo $itemID;?>' readonly>
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Item Name</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value='<?php echo $itemName;?>' readonly>
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Item Price</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value='Php. <?php echo $itemPrice;?>' readonly>
               </div>
          </div>

          <div class="row mb-2">
               <div class="col-sm-2">
                    <label class="form-label">Item Quantity</label>
               </div>

               <div class="col-sm-2">
                    <input type="text" class="form-control" value='<?php echo $itemQty;?>' readonly>
               </div>
          </div>
     </body>
</html>