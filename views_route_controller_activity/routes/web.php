<?php

use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{custID}/{custName}/{custAddress}', [PostsController::class, 'customer']);

Route::get('/item/{itemNo}/{itemName}/{itemPrice}', [PostsController::class, 'item']);

Route::get('/order/{custID}/{custName}/{orderNo}/{orderDate}', [PostsController::class, 'order']);

Route::get('/order-details/{transNo}/{orderNo}/{itemID}/{itemName}/{itemPrice}/{itemQty}', [PostsController::class, 'orderDetails']);