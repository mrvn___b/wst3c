<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>LOGIN | DR. CHU</title>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
          * {
               font-family: 'Poppins', sans-serif;
          }

          body {
               background-image: url("../images/bg.jpg");
               background-size: cover;
          }

          .error {
               margin-top: 125px;
               background-color: #ffffff;
          }

          a {
               color: #0e0057;
               text-decoration: none;
          }

          a:hover {
               color: #5727f5;
               text-decoration: underline;
          }

          .material-symbols-rounded {
               font-size: 80px;
               color: #5727f5;
               font-variation-settings:
                    'FILL' 1,
                    'wght' 400,
                    'GRAD' 0,
                    'opsz' 48
          }
        </style>
    </head>

    <body>
        <div class="d-flex justify-content-center">
            <div class="error d-inline-flex p-5 shadow">
                <div>
                     <div class="d-flex justify-content-center">
                          <div>
                              <span class="material-symbols-rounded">warning</span>
                          </div>
                     </div>

                     <p>The page you are trying to access is not available</p>
                     <p>Back to <a href="{{url ('/login')}}">Login</a></p>
                </div>
            </div>
        </div>
    </body>
</html>