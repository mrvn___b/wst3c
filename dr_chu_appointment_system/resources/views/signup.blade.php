<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SIGN UP | DR. CHU</title>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
          * {
               font-family: 'Poppins', sans-serif;
          }
          
          body {
               background-image: url("../images/bg.jpg");
               background-size: cover;
          }

          .btn-signup {
               background-color: #0e0057;
               color: #ffffff;
               width: 100%;
               margin-top: 30px;
          }

          .btn-signup:hover {
               background-color: #0c0142;
               color: #ffffff;
          }

          .btn-signup:focus {
               box-shadow: 0 0 0 0.2rem rgba(12, 1, 66, 0.25);
          }

          .form-control:focus {
               box-shadow: 0 0 0 0.2rem rgba(12, 1, 66, 0.25);
          }

          .login {
               margin-top: 80px;
               background-color: #ffffff;
          }

          a {
               color: #5727f5;
               text-decoration: none;
          }

          a:hover {
               color: #5727f5;
               text-decoration: underline;
          }

          p {
               margin-bottom: 0;
          }

          .error {
               color: red;
               font-size: 12px;
          }
        </style>
    </head>

    <body>
         <div class="d-flex justify-content-center">
            <div class="login d-inline-flex p-5 shadow">
                <div>
                    <div class="mb-3">
                        <h5>DR. CHU ONLINE APPOINTMENT SYSTEM</h5>
                        <h6 class="mt-3">SIGN UP</h6> 
                    </div>

                    <form action="/signup" method="POST">
                         {{ csrf_field() }}

                         <div class="row mt-3">
                              <div class="col-sm-4">
                                   <label for="username" class="form-label">Username</label>
                                   <input type="text" class="form-control" name="username" value="{{old ('username')}}">
                                   <p class="error">@error ('username'){{$message}} @enderror</p>
                              </div>

                              <div class="col-sm-8">
                                   <label for="email" class="form-label">Email</label>
                                   <input type="email" class="form-control" name="email" value="{{old ('email')}}">
                                   <p class="error">@error ('email'){{$message}} @enderror</p>
                              </div>
                         </div>

                         <div class="row mt-3">
                              <div class="col-sm-6">
                                   <label for="fname" class="form-label">First name</label>
                                   <input type="text" class="form-control" name="fname" value="{{old ('fname')}}">
                                   <p class="error">@error ('fname'){{$message}} @enderror</p>
                              </div>

                              <div class="col-sm-6">
                                   <label for="lname" class="form-label">Last name</label>
                                   <input type="text" class="form-control" name="lname" value="{{old ('lname')}}">
                                   <p class="error">@error ('lname'){{$message}} @enderror</p>
                              </div>
                         </div>

                         <label for="password" class="mt-3 form-label">Password</label>
                         <input type="password" class="form-control" name="password">
                         <p class="error">@error ('password'){{$message}} @enderror</p>

                         <input type="submit" class="btn btn-signup" name="submit" value="SIGN UP">
                    </form>

                    <p class="mt-4 mb-0">Already have an account? <a href="{{url ('/login')}}">Login</a>.</p>
                </div>
            </div>
          </div>
    </body>
</html>