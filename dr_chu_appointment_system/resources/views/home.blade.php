<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>HOME | DR. CHU</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
          * {
               font-family: 'Poppins', sans-serif;
          }

          .navbar {
               background-color: #0e0057;
          }

          .title {
               color: white;
          }

          .material-symbols-rounded {
               color: white;
               font-size: 30px;
               font-variation-settings:
                    'FILL' 1,
                    'wght' 400,
                    'GRAD' 0,
                    'opsz' 48
          }

          .btn-submit {
               background-color: #0e0057;
               color: #ffffff;
               margin-top: 32px;
          }

          .btn-submit:hover {
               color: #ffffff;
          }

          p {
                margin-bottom: 0;
            }

            .error {
                color: red;
                font-size: 12px;
            }

            .p2 {
                 width: 500px;
            }

            .name {
                 color: white;
                 margin-right: 10px;
                 margin-bottom: 0;
            }

            .section {
                 width: 70%;
                 height: 500px;
                 overflow-x: auto;
            }

            .card {
                 width: 200px;
            }

            .btn-danger {
                 width: 90px;
            }
        </style>
    </head>

    <body>
         <div class="navbar d-flex p-3">
              <div class="d-flex justify-content-between">
                   <h3 class="title mb-0">DR. CHU ONLINE APPOINTMENT SYSTEM</h3>
              </div>

              <div class="d-flex justify-content-between">
                    <span class="name">
                         @foreach($names as $name)
                                   {{$name->account_fname}}
                                   {{$name->account_lname}}
                         @endforeach
                    </span>

                    <a href="{{url ('/login')}}"><span class="material-symbols-rounded">logout</span></a>
              </div>
         </div>

              <form action="/insert" method="POST">
                    <div class="row m-3">
                         @if(Session::get('unsuccess'))
                            <div class = "alert alert-danger p-2">
                                <p>{{Session::get('unsuccess')}}</p>
                            </div>
                         @endif

                         @if(Session::get('success'))
                            <div class = "alert alert-success p-2">
                                <p>{{Session::get('success')}}</p>
                            </div>
                         @endif

                         @if(Session::get('deleted'))
                            <div class = "alert alert-danger p-2">
                                <p>{{Session::get('deleted')}}</p>
                            </div>
                         @endif

                         <h2>Set Appointment</h2>

                         @csrf

                         <input type="hidden" value="<?php echo request()->segment(2)?>" name="username">

                         <div class="col-sm-3 mt-3">
                              <label for="purpose" class="form-label">Purpose:</label>
                              <input type="text" class="form-control" name="purpose">
                              <p class="error">@error ('purpose'){{$message}} @enderror</p>
                         </div>

                         <div class="col-sm-2 mt-3">
                              <label for="date" class="form-label">Set date:</label>
                              <input type="date" class="form-control" name="date">
                              <p class="error">@error ('date'){{$message}} @enderror</p>
                         </div>

                         <div class="col-sm-2 mt-3">
                              <label for="time" class="form-label" name="time" aria-placeholder="Select">Set time:</label>
                              <select class="form-select" name="time">
                                   <option value=""></option>
                                   @foreach ($items as $time)
                                   <option value="{{$time->time_id}}">{{$time->time}}</option>
                                   @endforeach
                              </select>
                              <p class="error">@error ('time'){{$message}} @enderror</p>
                         </div>

                         <div class="col-sm-1 mt-3">
                              <input type="submit" class="btn btn-submit" value="SUBMIT">
                         </div>
                    </div>
               </form>

               <h2 class="m-3 mt-5">Appointments</h2>

               <div class="section ml-4 mt-4 p-3">

                    @foreach($app as $p)
                         <div class="card w-75 shadow mb-3" style="width: 18rem;">

                              <?php $status = $p->stat_id ?>

                              <div class="card-body">
                                   <h5 class="card-title mb-3">{{$p->app_purpose}}</h5>
                                   <p class="card-text">Date: {{$p->app_date}}</p>
                                   <p class="card-text">Time: {{$p->time}}</p>
                                   <p class="card-text mb-3">Status: {{$p->status}}</p>

                                   <form action="/delete" method="POST">
                                        @csrf
                                        
                                        <input type="hidden" name="app_id" value="{{$p->app_id}}">
                                        <div class="d-flex flex-row-reverse">
                                             @if($status == 0)              
                                                  <button type="submit" class="btn btn-danger">CANCEL</button>
                                             @endif

                                             @if($status == 1)  
                                                  <button type="submit" class="btn btn-secondary" disabled>CANCEL</button>
                                             @endif
                                        </div>
                                   </form>
                              </div>
                         </div>
                    @endforeach
               </div>
    </body>
</html>