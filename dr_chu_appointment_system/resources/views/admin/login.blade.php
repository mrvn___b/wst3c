<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>LOGIN | DR. CHU</title>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <style>
            * {
                font-family: 'Poppins', sans-serif;
            }

            body {
                background-color: #363636;
                background-size: cover;
            }

            .btn-login {
                background-color: #0e0057;
                color: #ffffff;
                width: 100%;
                margin-top: 30px;
            }

            .btn-login:hover {
                background-color: #0c0142;
                color: #ffffff;
            }

            .btn-login:focus {
                box-shadow: 0 0 0 0.2rem rgba(12, 1, 66, 0.25);
            }

            .form-control:focus {
                box-shadow: 0 0 0 0.2rem rgba(12, 1, 66, 0.25);
            }

            .login {
                margin-top: 125px;
                background-color: #ffffff;
            }

            a {
                color: #5727f5;
                text-decoration: none;
            }

            a:hover {
                color: #5727f5;
                text-decoration: underline;
            }

            .alert{
                margin-top: 10px;
                margin-bottom: 10px;
            }
            
            p {
                margin-bottom: 0;
            }
        </style>
    </head>

    <body>
        <div class="d-flex justify-content-center">
            <div class="login d-inline-flex p-5 shadow">
                <div>
                    <div class="mb-3">
                        <h5>DR. CHU ONLINE APPOINTMENT SYSTEM</h5>
                        <h6 class="mt-3">ADMINISTRATOR LOGIN</h6> 
                    </div>

                    @if (count($errors) > 0)
                            <div class = "alert alert-danger p-2">
                                <p>Incorrect username or password.</p>
                            </div>
                    @endif

                    <form action="/admin/home" method="POST">
                        @csrf
                        
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" name="username">

                        <label for="password" class="mt-2 form-label">Password</label>
                        <input type="password" class="form-control" name="password">

                        <input type="submit" class="btn btn-login" name="submit" value="LOGIN">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>