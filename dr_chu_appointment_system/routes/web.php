<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ValidationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//user

Route::get('/login', function () {
    return view('login');
});

Route::get('/signup', function () {
    return view('signup');
});

Route::post('/login', [
    ValidationController::class, "showform"
]);

Route::post('/home', [
    ValidationController::class, "validateform"
]);

Route::get('/home/{id}', [
    ValidationController::class, "userHome",
]);

Route::post('/signup', [
    ValidationController::class, "validateReg"
]);

Route::get('/login', [
    ValidationController::class, "backLogin"
]);

Route::post('/insert', [
    ValidationController::class, "addAppointment"
]);

Route::post('/delete', [
    ValidationController::class, "deleteAppointment"
]);



//admin

Route::get('/admin/login', function () {
    return view('admin.login');
});

Route::post('/admin/login', [
    ValidationController::class, "showAdminForm"
]);

Route::post('/admin/home', [
    ValidationController::class, "validateAdmin"
]);

Route::get('/admin/home/{id}', [
    ValidationController::class, "adminHome"
]);

Route::post('/update', [
    ValidationController::class, "updateAdmin"
]);