<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;


class ValidationController extends Controller {
    public function showform() {
        return view('login');
    }

    public function validateform(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = DB::table('tbl_users')->where('account_username', $username)->
        where('account_password', $password)->pluck('account_username');

        if($user) {
            $p1 = str_replace("[", "", $user);
            $p2 = str_replace("]", "", $p1);
            $p3 = str_replace('"', "", $p2);

            return redirect('/home/'.$p3);
        }

        return back()->with('invalid', 'Incorect username or password.');

    }

    public function showAdminForm() {
        return view('admin.login');
    }

    public function validateAdmin(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = DB::table('tbl_users')->where('account_username', $username)->
        where('account_password', $password)->pluck('account_username');

        if($user) {
            $p1 = str_replace("[", "", $user);
            $p2 = str_replace("]", "", $p1);
            $p3 = str_replace('"', "", $p2);

            return redirect('admin/home/'.$p3);
        }

        return back()->with('invalid', 'Incorect username or password.');
    }

    public function updateAdmin(Request $request) {
        $app_id = $request->input('app_id');

        $update = DB::update('UPDATE tbl_appointment SET app_status = ? where app_id = ?', [1, $app_id]);

        if($update) {
            return back()->with('success', 'Appointment completed.');
        }
    }

    public function adminHome() {
        $user = request()->segment(3);

        $data = array(
            'names'=>DB::table('tbl_users')->where('account_username', $user)->get(),
            'app'=>DB::table('tbl_appointment')->join('tbl_time', 'tbl_time.time_id', '=', 'tbl_appointment.app_time')->join('status', 'tbl_appointment.app_status', '=', 'status.stat_id')->join('tbl_users', 'tbl_appointment.account_id', '=', 'tbl_users.account_id')->get()
        );

        return view ('/admin/home',$data);
    }

    public function userHome() {
        $user = request()->segment(2);

        $id = DB::table('tbl_users')->where('account_username', $user)->pluck('account_id');

        $q1 = str_replace("[", "", $id);
        $q2 = str_replace("]", "", $q1);
        $q3 = str_replace('"', "", $q2);

        $data = array(
            'items'=>DB::table('tbl_time')->get(),
            'names'=>DB::table('tbl_users')->where('account_username', $user)->get(),
            'app'=>DB::table('tbl_appointment')->join('tbl_time', 'tbl_time.time_id', '=', 'tbl_appointment.app_time')->join('status', 'tbl_appointment.app_status', '=', 'status.stat_id')->where('account_id', $q3)->get()
        );

        return view ('/home',$data);
    }

    public function addAppointment(Request $request) {
        $request->validate([
            'purpose' => 'required',
            'date' => 'required|after:today',
            'time' => 'required',
        ]);

        $username = $request->input('username');

        $id = DB::table('tbl_users')->where('account_username', $username)->pluck('account_id');

        $f = str_replace("[", "", $id);
        $f1 = str_replace("]", "", $f);
        $f2 = str_replace('"', "", $f1);

        $date = $request->input('date');
        $time = $request->input('time');

        $check = DB::table('tbl_appointment')->where('app_date', $date)->where('app_time', $time)->where('app_status', 0)->first();

        if($check) {
            return back()->with('unsuccess', 'Appointment unavaible.');
        }

        else {
            DB::table('tbl_appointment')->insert([
                'account_id'=>$f2,
                'app_purpose'=>$request->input('purpose'),
                'app_date'=>$date,
                'app_time'=>$time,
            ]);

            return back()->with('success', 'Appointment added.');
        }
    }

    public function deleteAppointment(Request $request) {
        $app_id = $request->input('app_id');

        $delete = DB::table('tbl_appointment')->where('app_id', $app_id)->delete();

        if($delete) {
            return back()->with('deleted', 'Appointment cancelled.');
        }
    }


    public function validateReg(Request $request) {
        $request->validate([
            'username' => 'required|max:8|unique:tbl_users,account_username',
            'email' => 'required|unique:tbl_users,account_email',
            'fname' => 'required',
            'lname' => 'required',
            'password' => 'required|min:8'
        ]);

        DB::table('tbl_users')->insert([
            'account_username'=> $request-> input('username'),
            'account_email'=> $request-> input('email'),
            'account_fname'=> $request-> input('fname'),
            'account_lname' =>  $request-> input('lname'),
            'account_password' =>  $request-> input('password'),
        ]);

        return redirect('/login')->with('success', 'Account created!');
    }

    public function backLogin() {
        return view('login');
    }
}
