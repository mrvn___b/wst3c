<?php
    if(isset($_GET['code'])){
        require_once 'vendor/autoload.php';

        $client_id = '373413669077-t140ekalnu3e4qe05avtbsiq8o55r115.apps.googleusercontent.com';
        $client_secret = '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';
        $redirect_uri = 'http://localhost/login_api/home.php';

        $client = new Google_Client();
        $client->setClientID($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope('profile');
        $client->addScope('email');

        $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $client->setAccessToken($token);

        $gauth = new Google_Service_Oauth2($client);
        $google_info = $gauth->userinfo->get();
        $email = $google_info->email;
        $name = $google_info->name;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <script src="https://kit.fontawesome.com/091cd4a75a.js" crossorigin="anonymous"></script>
        <title>HOME | LOGIN API</title>
    </head>

    <body>
        <div class="main">
            <div>
                <table>
                    <tr>
                        <td>Name from Google: <?php echo $name?></td>
                        <td><?php echo date('Y/m/d');?></td>
                    </tr>

                    <tr>
                        <td>Year and Section: 3-C</td>
                        <td><?php echo date('h:i:sa');?></td>
                    </tr>
                </table>

                <h2 class="welcome">WELCOME TO GOOGLE</h2>

                <div class="social_apps">
                    <div>
                        <a class="google" href="logout.php"><button class ="btn btn-google"><span>LOGOUT</span></i></button></a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>