<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>REGISTER</title>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body>
     <div class="d-flex jsutify-content-center align-items-center">
          <div>
               @if(Session::get('success'))
                    <div class = "alert alert-success p-2">
                         <p>{{Session::get('success')}}</p>
                    </div>
               @endif
               <h2>REGISTER</h2>

               <form action="/new-user" method="POST">
                    @csrf
                    
                    <input type="text" class="form-control mt-3" name="username" placeholder="Username">
                    <input type="email" class="form-control mt-3" name="email" placeholder="Email">
                    <input type="password" class="form-control mt-3" name="password" placeholder="Password">

                    <input type="submit" class="btn btn-primary mt-3" name="submit" value="REGISTER">
               </form>

               <div class="mt-3">
                    Don't have an account? <a href="{{ url('/login') }}">Login</a>
               </div>
          </div>
     </div>
</body>
</html>