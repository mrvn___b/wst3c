<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <title>HOME</title>
     <style>
          * {
               margin: 0;
               padding: 0;
               font-family: 'Poppins', sans-serif;
          }

          .nav {
               display: flex;
               justify-content: space-between;
               align-items: center;
               padding: 1rem 5rem;
               background-color: #363636;
               color: #FFFFFF;
          }
     </style>
</head>
<body>
     <div class="nav">
          <div>
               <h2>Supply System</h2>
          </div>

          <div>
               <a href="{{ url('/login') }}">Logout</a>
          </div>
     </div>

     <div class="m-5">
          @foreach($item as $i)
               <div class="card shadow mb-3" style="width: 40rem;">
                    <div class="card-body">
                         <h5 class="card-title">Item: {{$i->name}}</h5>
                         <p class="card-text">Price: {{$i->price}}</p>
                         <p class="card-text">Stocks: @if($i->stocks >=1) {{$i->stocks}} @endif @if($i->stocks == 0) Out of stock @endif</p>
                         <p class="card-text">Date Purchased: {{$i->date_purchased}}</p>

                         <div class="d-flex justify-content-end">
                              <form action="/delete-faqs" method="POST">
                                   @csrf

                                   <input type="hidden" name="product_id" value="{{$i->product_id}}">
                                        <div style="margin-left: 10px;">
                                             <button 
                                                  type="submit" 
                                                  class="btn btn-primary"
                                                  @if($i->stocks == 0)
                                                       disabled
                                                  @endif
                                             >
                                                  <div class="d-flex align-items-center justify-content-center">
                                                       <div class="d-flex align-items-start">
                                                            <div>
                                                                 REQUEST TO AVAIL
                                                            </div>
                                                       </div>
                                                  </div>
                                             </button>
                                        </div>
                              </form>
                         </div>
                    </div>
               </div>
          @endforeach
     </div>


</body>
</html>