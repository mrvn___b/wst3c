<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function view() {
        return view('home');
    }

    public function newUser(Request $request) {
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');

        DB::table('users')->insert([
            'username'=>$username,
            'email'=>$email,
            'password'=>$password,
        ]);

        return back()->with('success', 'Successfully added');
    }

    public function itemView() {
        $data = array(
            'item'=>DB::table('items')->get()
        );

        return view('home', $data);
    }
}
