<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function loggedIn(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');

        $user = DB::table('users')->where('username', $username)->where('password', $password);

        if($user->count() == 1) {
            
            return redirect('/home');
        }
    }
}
